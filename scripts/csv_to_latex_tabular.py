import sys, os
import pandas as pd
import numpy as np

# outputs tabular inside directory of the input file

if len(sys.argv) < 2:
    print("Input argument (a path to .csv file) must be given.")
    sys.exit()

inputPath = sys.argv[1]

try:
    file = open(inputPath, "r")
except FileNotFoundError:
    print("File '%s' does not exist." % (inputPath,))
    sys.exit()


splittedInputPath = inputPath.split("/")
splittedInputPath[-1] = "tabular_of_results"
outputPath = "/".join(splittedInputPath)

slash = "\ "[:1]
dslash = slash + slash
indent = "    "
hline = slash + "hline"
br = dslash + "\n" + indent
brWithHline = dslash + "\n" + indent + hline + "\n" + indent

tabular = slash + "begin{center}\n" + indent
tabular += "\makebox[0pt]{\n" + indent
tabular += slash + "begin{tabular}{l r r r r}\n%s%s\n%s"\
    % (indent,hline,indent)
tabular += "Algorytm & \makecell{Rozmiar po %s kompresji} \n%s"\
    % (dslash, indent) 
tabular += "& \makecell{Wysokość %s gramatyki} & \makecell{Czas kompresji %s (w ms)}"\
    % (dslash, dslash)
tabular += "\n%s & \makecell{Maksymalne zużycie %s pamięci (w kB)}%s%s\n%s" \
        % (indent, dslash, dslash, "[0.5ex]", indent) 
# tabular += hline + "\n" + indent

table = pd.read_csv(inputPath)
table["time"] = np.array(np.floor(table["time"]), dtype=int)

def putCommasInNumber(number):
    revnumber = str(number)[::-1]
    parts = [revnumber[3*i:3*(i+1)][::-1]\
        for i in range(int(np.ceil(len(revnumber) / 3)))]
    return ".".join(parts[::-1])

for fname in table["file"].unique():
    fnameRows = table[table["file"] == fname]
    compressedFileLength = fnameRows["input size"].unique()[0]
    tabular += "%s\n%s\multicolumn{5}{c}{%stextbf{Plik: ,,%s'', długość: %s}}%s"\
        % (hline, indent, slash, fname,\
            putCommasInNumber(compressedFileLength), brWithHline)

    rowsForFname = fnameRows[["algorithm", "output size", "max height",\
        "time", "memory"]]
    for row in rowsForFname.values:
        for i in range(row.size):
            cell = row[i]
            if row[0][:2] == "LZ" and i == 2:
                cell = "---"
            if type(cell) == str:
                tabular += cell + " & "
            else:
                tabular += putCommasInNumber(cell) + " & "
        tabular = tabular[:-2] + br

tabular += hline + "\n"
tabular += indent + slash + "end{tabular}}\n"
tabular += slash + "end{center}"

file.close()

print(tabular)
