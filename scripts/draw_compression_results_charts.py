import sys, os, json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

scriptPath = os.path.abspath(os.path.dirname(sys.argv[0]))
projectPath = scriptPath + "/../"

if len(sys.argv) < 2:
    print("Input argument (a path to directory with results of " \
        + "'run_compression.py') must be given.")
    sys.exit()

inputDirPath = sys.argv[1]

if not os.path.exists(inputDirPath):
    print("Path '%s' does not exist." % (inputDirPath,))
    sys.exit()

outputDirName = "compression_results_charts"
outputDirPath = projectPath + outputDirName + "/"
if not os.path.exists(outputDirPath):
    os.mkdir(outputDirPath)


dirsWithResultsForFile = \
    [(f, os.path.join(inputDirPath, f)) for f in os.listdir(inputDirPath) \
        if os.path.isdir(os.path.join(inputDirPath, f))]



# parse output of run_compression.py script
allResults = pd.DataFrame([], columns=["file", "algorithm", "alpha", "time",\
    "memory", "input size", "output size", "max height"])
for resultsDirName, resultsDirPath in dirsWithResultsForFile:
    compressedFileName = resultsDirName[4:]
    dirsWithResultsForAlgorithm = [(f, os.path.join(resultsDirPath, f))\
        for f in os.listdir(resultsDirPath) \
        if os.path.isdir(os.path.join(resultsDirPath, f))]
    for algResultsDirName, algResultsDirPath in dirsWithResultsForAlgorithm:
        # parse name from a directory 'out_<algorithm_name>'
        nameAndAlpha = algResultsDirName.split("--alpha")
        algorithmName = nameAndAlpha[0][4:]
        # directories related to the Alpha algorithm have a parameter in name
        if len(nameAndAlpha) == 2:
            alpha = float(nameAndAlpha[1])
            algorithmName += " " + nameAndAlpha[1]
        else:
            alpha = 0.
        generalStatsFilesPaths = [os.path.join(algResultsDirPath, f) \
            for f in os.listdir(algResultsDirPath) \
            if f[:13] == "general_stats"]
        
        # collect compression times
        times = []
        for generalStatsFile in generalStatsFilesPaths:
            parsedJson = json.load(open(generalStatsFile))
            times.append(parsedJson["Compression time (ms)"])
            inputSize = parsedJson["Input text length"]
            maxHeight = parsedJson["Max height"]
            outputSize = parsedJson["Size"]

        memoryRecord = open(algResultsDirPath + "/memory_record").read()
        # parse memory_record file - get only the line with max memory usage
        memoryRecord = [line for line in memoryRecord.split("\n") \
            if "Maximum resident set size" in line][0]
        # parse the usage value
        memoryRecord = int(memoryRecord.split(" ")[-1])
        allResults = allResults.append({ "file": compressedFileName, "algorithm": algorithmName,\
            "alpha": alpha, "time": np.mean(times), "memory": memoryRecord,\
            "input size": inputSize, "output size": outputSize,\
            "max height": maxHeight }, ignore_index=True)
        
allResults.alpha = pd.to_numeric(allResults.alpha)
allResults.time = pd.to_numeric(allResults.time)
allResults.memory = pd.to_numeric(allResults.memory)
allResults["input size"] = pd.to_numeric(allResults["input size"])
allResults["output size"] = pd.to_numeric(allResults["output size"])
allResults["max height"] = pd.to_numeric(allResults["max height"])


allResults[["algorithm", "file", "input size", "output size", "max height",\
    "time", "memory"]]\
    .sort_values(["algorithm", "input size"])\
    .to_csv(outputDirPath + "/results.csv", ",", index=False)


if allResults["input size"].min() >= 1000000:
    allResults["file"] += " (" + (allResults["input size"] \
        // 100000 / 10).astype(str) + " MB)"
elif allResults["input size"].min() >= 1000:
    allResults["file"] += " (" + (allResults["input size"] \
        // 100 / 10).astype(str) + " kB)"
else:
    allResults["file"] += " (" + (allResults["input size"]).astype(str)\
        + " bytes)"


# draw charts

colors = { "AVL" : "red", "Alpha 0.292": "green", "Alpha 0.15": "purple", \
    "Alpha 0.01": "violet", "Recompress": "brown", "Simple": "orange", \
    "LZ1": "blue", "LZ77": "pink"}

points = { "AVL" : "^", "Alpha 0.292": "v", "Alpha 0.15": "v", \
    "Alpha 0.01": "v", "Recompress": "s", "Simple": "o", "LZ1": "x", \
    "LZ77": "D"}

titles = { "time": "Czas kompresji", "max height": "Wysokość gramatyki", \
    "memory": "Maksymalne zużycie pamięci", \
    "output size": "Rozmiar po kompresji" }

def formatValueInMilliseconds(value, upperBound):
    if upperBound >= 20000:
        return str(int(value // 1000)) + " s"
    if upperBound >= 2000:
        return str(value // 100 / 10) + " s"
    return str(int(value)) + " ms"
    
def formatValueInKilobytes(value, upperBound):
    if upperBound >= 4000000:
        return str(int(value // 1000000)) + " GB"
    if upperBound >= 2000000:
        return str(value // 100000 / 10) + " GB"
    if upperBound >= 20000:
        return str(int(value // 1000)) + " MB"
    if upperBound >= 2000:
        return str(value // 100 / 10) + " MB"
    return str(int(value)) + " kB"

def putCommasInNumber(number):
    revnumber = str(number)[::-1]
    parts = [revnumber[3*i:3*(i+1)][::-1]\
        for i in range(int(np.ceil(len(revnumber) / 3)))]
    return ".".join(parts[::-1])

def drawChart(yParameter, algorithmsToIgnore):
    resultsToDraw = allResults.sort_values(["input size", "file"])\
        [["algorithm", "file", yParameter]]
    resultsToDraw = resultsToDraw[~(resultsToDraw.algorithm\
        .isin(algorithmsToIgnore))]
    fig, ax = plt.subplots(figsize=(10, 5))
    for algorithm in resultsToDraw.algorithm.sort_values().unique():
        algorithmResults = resultsToDraw[resultsToDraw.algorithm == algorithm]
        style = "-"
        if algorithm[:5] == "Alpha":
            style = "--"
        plt.plot(algorithmResults["file"], algorithmResults[yParameter],\
            points[algorithm], linestyle=style, label=algorithm,\
            color=colors[algorithm])
    plt.legend(loc="best")
    plt.title(titles[yParameter])

    if yParameter == "time":
        ax.yaxis.set_major_formatter(FuncFormatter(\
            lambda x, pos: formatValueInMilliseconds(x, \
                resultsToDraw.time.max())))
    if yParameter == "memory":
        ax.yaxis.set_major_formatter(FuncFormatter(\
            lambda x, pos: formatValueInKilobytes(x, \
                resultsToDraw.memory.max())))

    if yParameter == "max height":
        ax.set_ylim(0.9 * resultsToDraw[yParameter].min(),\
            np.min((100, 1.1 * resultsToDraw[yParameter].max())))
    
    if yParameter == "output size":
        ax.yaxis.set_major_formatter(FuncFormatter(\
            lambda x, pos: putCommasInNumber(int(x))))

    plt.savefig("%s/%s" % (outputDirPath, yParameter.replace(" ", "_")), dpi=150, bbox_inches="tight")
    plt.close()

drawChart("output size", ["LZ77"])
drawChart("memory", ["LZ77"])
drawChart("time", [])
drawChart("max height", ["LZ1", "LZ77"])
