import sys, os


# algorithms to test; differrent alpha parameter values for algorithm A2
algorithms = ["AVL", "Alpha --alpha 0.01", "Alpha --alpha 0.15", \
    "Alpha --alpha 0.292", "Recompress", "Simple", "LZ1", "LZ77"]


# compression with generation general stats should be repeated, 
#   because compression's duration may differ (and should be averaged);
#   after all, it is once again performed without generating any output files,
#   to measure max memory usage during compression
timesToRepeat = 3


scriptPath = os.path.abspath(os.path.dirname(sys.argv[0]))
projectPath = scriptPath + "/../"
compressionProgramPath = projectPath + "GrammarCompression"

if not os.path.isfile(compressionProgramPath):
    print("GrammarCompression program has not been found!")
    sys.exit()


if len(sys.argv) < 2:
    print("Input argument (a path to directory with files to compress) " \
        + "must be given.")
    sys.exit()

inputDirPath = sys.argv[1]

if not os.path.exists(inputDirPath):
    print("Path '%s' does not exist." % (inputDirPath,))
    sys.exit()

outputDirName = "compressed_data"
outputDirPath = projectPath + outputDirName
if not os.path.exists(outputDirPath):
    os.mkdir(outputDirPath)


filesToCompress = \
    [(f, os.path.join(inputDirPath, f)) for f in os.listdir(inputDirPath) \
        if os.path.isfile(os.path.join(inputDirPath, f))]



# compression
for (fileName, pathToFile) in filesToCompress:
    # dir for outputs of compression of the input file
    compressedFileOutputDirPath = "%s/out_%s/" % (outputDirPath, fileName)
    if not os.path.exists(compressedFileOutputDirPath):
        os.mkdir(compressedFileOutputDirPath)
    for algorithm in algorithms:
        # dir for the algorithm's outputs
        algorithmOutputDirPath =  "%s/out_%s/" \
            % (compressedFileOutputDirPath, algorithm.replace(" ", ""))
        if not os.path.exists(algorithmOutputDirPath):
            os.mkdir(algorithmOutputDirPath)

        # run many times and save all generated general stats
        for i in range(timesToRepeat):
            os.system("%s --compr %s --gstat T --out %s %s" \
                % (compressionProgramPath, algorithm, \
                    algorithmOutputDirPath, pathToFile))
            os.rename(algorithmOutputDirPath + "general_stats.json", \
                "%s/general_stats%s.json" % (algorithmOutputDirPath, i))

        # run and check max memory usage
        memoryRecordPath = algorithmOutputDirPath + "memory_record"
        os.system("/usr/bin/time -o %s --verbose %s --compr %s --grdef F %s" \
            % (memoryRecordPath, compressionProgramPath, algorithm, pathToFile))

