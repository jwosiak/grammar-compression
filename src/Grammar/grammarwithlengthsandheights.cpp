#include "grammarwithlengthsandheights.h"
#include <cmath>

GrammarWithLengthsAndHeights::GrammarWithLengthsAndHeights()
    : GrammarWithLengths()
{

}

void GrammarWithLengthsAndHeights::clearCache()
{
    heights.clear();
}

uint8_t GrammarWithLengthsAndHeights::getHeight(index_t symbol) const
{
    if (symbol <= getMaxTerminal())
        return 0;
    return heights[getNonterminalPos(symbol)];
}

index_t GrammarWithLengthsAndHeights::newNonterminal(production_t &prod)
{
    index_t freshNonterminal = GrammarWithLengths::newNonterminal(prod);
    heights.push_back(0);
    updateHeight(freshNonterminal);
    return freshNonterminal;
}

void GrammarWithLengthsAndHeights::setProd(
        index_t nonterminal, production_t &prod)
{
    GrammarWithLengths::setProd(nonterminal, prod);
    updateHeight(nonterminal);
}

void GrammarWithLengthsAndHeights::setProd(
        index_t nonterminal, production_t &&prod)
{
    GrammarWithLengths::setProd(nonterminal, prod);
    updateHeight(nonterminal);
}

bool GrammarWithLengthsAndHeights::areBalanced(
        index_t leftProd, index_t rightProd) const
{
    return std::abs(getHeight(leftProd) - getHeight(rightProd)) <= 1;
}

void GrammarWithLengthsAndHeights::updateHeight(index_t nonterminal)
{
    production_t prod = getProd(nonterminal);
    heights[getNonterminalPos(nonterminal)] =
            std::max(getHeight(prod.first), getHeight(prod.second)) + 1;
}

void GrammarWithLengthsAndHeights::resize(index_t nonterminalsNum)
{
    GrammarWithLengths::resize(nonterminalsNum);
    heights.resize(nonterminalsNum);
}

