#include "grammarstatsevaluator.h"
#include "grammar.h"

GrammarStatsEvaluator::GrammarStatsEvaluator(const Grammar &grammar)
    : grammar(grammar)
{

}

vectormap GrammarStatsEvaluator::produceStats() const
{
    index_t lettersNum = grammar.size();
    std::vector<index_t> startedAt(lettersNum);
    std::vector<index_t> count(lettersNum);
    auto *lengths = new std::vector<index_t>(lettersNum);
    auto *heights = new std::vector<index_t>(lettersNum);

    for (index_t i = 0; i <= grammar.getMaxTerminal(); i++)
        lengths->at(i) = 1;

    produceStatsRecPart(lettersNum-1, 0, startedAt, *lengths, count, *heights);

    startedAt.clear();

    auto accumulatedCount = new std::vector<index_t>(lettersNum);
    accumulatedCount->back() = 1;

    for (index_t X = lettersNum-1; X > grammar.getMaxTerminal(); X--) {

        indexpair_t prod = grammar.getProd(X);
        index_t cnt = accumulatedCount->at(X);

        accumulatedCount->at(prod.first) += cnt;
        accumulatedCount->at(prod.second) += cnt;
    }

    count.clear();

    vectormap stats;
    stats["Length"] = lengths;
    stats["Count"] = accumulatedCount;
    stats["Height"] = heights;
    return stats;
}

void GrammarStatsEvaluator::produceStatsRecPart(
        index_t a, index_t pos, std::vector<index_t> &startedAt,
        std::vector<index_t> &lengths, std::vector<index_t> &count,
        std::vector<index_t> &heights) const
{
    count[a]++;
    if (lengths[a] > 0)
        return;

    if (a <= grammar.getMaxTerminal()) {
        startedAt[a] = pos;
        lengths[a] = 1;
        heights[a] = 0;
        return;
    }

    production_t prod = grammar.getProd(a);
    produceStatsRecPart(prod.first, pos, startedAt, lengths, count, heights);

    produceStatsRecPart(prod.second, pos+lengths[prod.first],
            startedAt, lengths, count, heights);

    startedAt[a] = pos;
    lengths[a] = lengths[prod.first] + lengths[prod.second];
    heights[a] = std::max(heights[prod.first], heights[prod.second]) + 1;
}
