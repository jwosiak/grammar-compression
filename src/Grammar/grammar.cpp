#include "grammar.h"
#include <stack>

const std::vector<production_t> &Grammar::getProductions() const
{
    return productions;
}

void Grammar::resize(index_t nonterminalsNum)
{
    productions.resize(nonterminalsNum);
}

std::vector<bool> Grammar::getUsedNonterminalsMask()
{
    std::vector<bool> usedNonterminals(productions.size());
    std::stack<index_t> toVisit;
    toVisit.push(getMaxSymbol());

    while (!toVisit.empty()) {
        index_t topNonterminal = toVisit.top();
        toVisit.pop();
        if (topNonterminal > maxTerminal) {
            index_t nonterminalId = getNonterminalPos(topNonterminal);
            if (!usedNonterminals[nonterminalId]) {
                usedNonterminals[nonterminalId] = true;
                indexpair_t prod = getProd(topNonterminal);
                toVisit.push(prod.second);
                toVisit.push(prod.first);
            }
        }
    }

    return usedNonterminals;
}

std::vector<production_t> &Grammar::putUsedNonterminalsInContinuousSequence(
        std::vector<bool> &usedNonterminalsMask)
{
    index_t usedNonterminalsCount = maxTerminal + 1;
    std::vector<index_t> oldToNewPositions(productions.size());

    for (index_t nonterminalsIter = 0; nonterminalsIter < productions.size()
         ; nonterminalsIter++) {
        if (usedNonterminalsMask[nonterminalsIter]) {
            production_t prod = productions[nonterminalsIter];
            index_t leftProd = prod.first <= maxTerminal
                    // terminals are not moved
                    ? prod.first
                    : oldToNewPositions[getNonterminalPos(prod.first)];
            index_t rightProd = prod.second <= maxTerminal
                    ? prod.second
                    : oldToNewPositions[getNonterminalPos(prod.second)];

//            productions[getNonterminalPos(usedNonterminalsCount)] =
//                    indexpair_t(leftProd, rightProd);
            setProd(usedNonterminalsCount, production_t(leftProd, rightProd));
            oldToNewPositions[nonterminalsIter] = usedNonterminalsCount++;
        }
    }

    resize(usedNonterminalsCount - maxTerminal - 1);
    return productions;
}

Grammar::Grammar()
{
    
}

Grammar::Grammar(const std::vector<production_t> &productions)
    : productions(productions)
{

}

Grammar::~Grammar()
{

}

void Grammar::clearCache()
{

}

terminal_t Grammar::getMaxTerminal() const
{
    return maxTerminal;
}

index_t Grammar::size() const
{
    return maxTerminal + productions.size() + 1;
}

index_t Grammar::getMaxSymbol() const
{
    return size()-1;
}

index_t Grammar::getNonterminalPos(index_t nonterminal) const
{
    return nonterminal - maxTerminal - 1;
}

production_t Grammar::getProd(index_t nonterminal) const
{
    return productions[getNonterminalPos(nonterminal)];
}

index_t Grammar::getEmptyLetter() const
{
    return getMaxSymbol() + 1;
}

index_t Grammar::newNonterminal(production_t &prod)
{
    productions.push_back(prod);
    return getMaxSymbol();
}

index_t Grammar::newNonterminal(production_t &&prod)
{
    return newNonterminal(prod);
}

index_t Grammar::newNonterminal(index_t lProd, index_t rProd)
{
    return newNonterminal(production_t(lProd, rProd));
}

void Grammar::setProd(index_t nonterminal, production_t &prod)
{
    productions[getNonterminalPos(nonterminal)] = prod;
}

void Grammar::setProd(index_t nonterminal, production_t &&prod)
{
    productions[getNonterminalPos(nonterminal)] = prod;
}

void Grammar::swapPositionsInHierarchy(
        index_t &nonterminal1, index_t &nonterminal2)
{
    std::swap(productions[getNonterminalPos(nonterminal1)],
            productions[getNonterminalPos(nonterminal2)]);
    std::swap(nonterminal1, nonterminal2);
}

void Grammar::removeUnusedNonterminals()
{
    std::vector<bool> usedNonterminalsMask = getUsedNonterminalsMask();
    productions =
            putUsedNonterminalsInContinuousSequence(usedNonterminalsMask);
}

std::vector<terminal_t> Grammar::produceWord() const
{
    return evalSymbol(getMaxSymbol());
}

std::vector<terminal_t> Grammar::evalSymbol(index_t symbol) const
{
    std::vector<terminal_t> word;

    if (symbol <= maxTerminal) {
        word.push_back(symbol);
        return word;
    }

    index_t lettersNum = size();
    std::vector<index_t> startedAt(lettersNum);
    std::vector<index_t> lengths(lettersNum);

    produceWordRecPart(symbol, 0, startedAt, lengths, word);
    return word;
}

void Grammar::produceWordRecPart(
        index_t a, index_t pos, std::vector<index_t> &startedAt,
        std::vector<index_t> &lengths, std::vector<terminal_t> &word) const
{
    index_t len = lengths[a];
    if (len > 0) {

        index_t beginning = startedAt[a];
        for (index_t j = beginning; j < beginning + len; j++) {

            word.push_back(word[j]);
        }
        return;
    }

    if (a <= maxTerminal) {
        word.push_back(a);
        startedAt[a] = pos;
        lengths[a] = 1;
        return;
    }

    production_t prod = productions[a-maxTerminal-1];
    produceWordRecPart(prod.first, pos, startedAt, lengths, word);

    produceWordRecPart(prod.second, pos+lengths[prod.first],
            startedAt, lengths, word);

    startedAt[a] = pos;
    lengths[a] = lengths[prod.first] + lengths[prod.second];
}

