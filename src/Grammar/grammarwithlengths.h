#ifndef GRAMMARWITHLENGTHS_H
#define GRAMMARWITHLENGTHS_H

#include "grammar.h"
#include "types.h"

// contains length of each nonterminal

class GrammarWithLengths : public Grammar
{
    std::vector<index_t> lengths;
public:
    GrammarWithLengths();
    virtual ~GrammarWithLengths();

    virtual void clearCache();

    using Grammar::newNonterminal;
    virtual index_t newNonterminal(production_t &prod);
    unsigned int getLength(index_t symbol) const;

    virtual void setProd(index_t nonterminal, production_t &prod);
    virtual void setProd(index_t nonterminal, production_t &&prod);
    virtual void swapPositionsInHierarchy(
            index_t &nonterminal1, index_t &nonterminal2);

private:
    void updateLength(index_t nonterminal);

protected:
    virtual void resize(index_t nonterminalsNum);
};

#endif // GRAMMARWITHLENGTHS_H
