#include "grammarwithlengths.h"

GrammarWithLengths::GrammarWithLengths()
    : Grammar()
{

}

GrammarWithLengths::~GrammarWithLengths()
{

}

void GrammarWithLengths::clearCache()
{
    lengths.clear();
}

index_t GrammarWithLengths::newNonterminal(production_t &prod)
{
    index_t newProd = Grammar:: newNonterminal(prod);
    lengths.push_back(getLength(prod.first) + getLength(prod.second));
    return newProd;
}

unsigned int GrammarWithLengths::getLength(index_t symbol) const
{
    if (symbol <= getMaxTerminal())
        return 1;

    return lengths[getNonterminalPos(symbol)];
}

void GrammarWithLengths::setProd(index_t nonterminal, production_t &prod)
{
    Grammar::setProd(nonterminal, prod);
    updateLength(nonterminal);
}

void GrammarWithLengths::setProd(index_t nonterminal, production_t &&prod)
{
    Grammar::setProd(nonterminal, prod);
    updateLength(nonterminal);
}

void GrammarWithLengths::swapPositionsInHierarchy(
        index_t &nonterminal1, index_t &nonterminal2)
{
    Grammar::swapPositionsInHierarchy(nonterminal1, nonterminal2);
    std::swap(lengths[getNonterminalPos(nonterminal1)],
            lengths[getNonterminalPos(nonterminal2)]);
}

void GrammarWithLengths::updateLength(index_t nonterminal)
{
    production_t prod = getProd(nonterminal);
    lengths[getNonterminalPos(nonterminal)] =
            getLength(prod.first) + getLength(prod.second);
}

void GrammarWithLengths::resize(index_t nonterminalsNum)
{
    Grammar::resize(nonterminalsNum);
    lengths.resize(nonterminalsNum);
}
