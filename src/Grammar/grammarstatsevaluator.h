#ifndef GRAMMARSTATSEVALUATOR_H
#define GRAMMARSTATSEVALUATOR_H

#include "types.h"

class Grammar;

class GrammarStatsEvaluator
{
    const Grammar& grammar;
public:
    GrammarStatsEvaluator(const Grammar& grammar);

    vectormap produceStats() const;

private:
    void produceStatsRecPart(
            index_t a, index_t pos, std::vector<index_t> &startedAt,
            std::vector<index_t> &lengths, std::vector<index_t> &count,
            std::vector<index_t> &heights) const;

};

#endif // GRAMMARSTATSEVALUATOR_H
