#ifndef GRAMMAR_H
#define GRAMMAR_H

#include "types.h"
#include <vector>

class Grammar
{
    terminal_t maxTerminal = 255;
    std::vector<production_t> productions;

public:
    Grammar();
    Grammar(const std::vector<production_t> &productions);
    virtual ~Grammar();

    virtual void clearCache();

    terminal_t getMaxTerminal() const;
    index_t size() const;
    index_t getMaxSymbol() const;
    // returns position in 'productions'
    index_t getNonterminalPos(index_t nonterminal) const;
    production_t getProd(index_t nonterminal) const;
    // a letter which is neither a terminal nor a production
    index_t getEmptyLetter() const;

    virtual index_t newNonterminal(production_t &prod);
    virtual index_t newNonterminal(production_t &&prod);
    virtual index_t newNonterminal(index_t lProd, index_t rProd);
    virtual void setProd(index_t nonterminal, production_t &prod);
    virtual void setProd(index_t nonterminal, production_t &&prod);
    virtual void swapPositionsInHierarchy(
            index_t &nonterminal1, index_t &nonterminal2);

    void removeUnusedNonterminals();
    std::vector<terminal_t> produceWord() const;
    std::vector<terminal_t> evalSymbol(index_t symbol) const;

    const std::vector<production_t>& getProductions() const;
protected:
    virtual void resize(index_t nonterminalsNum);
private:
    std::vector<bool> getUsedNonterminalsMask();
    std::vector<production_t> &putUsedNonterminalsInContinuousSequence(
            std::vector<bool> &usedNonterminalsMask);

    void produceWordRecPart(
            index_t a, index_t pos, std::vector<index_t> &startedAt,
            std::vector<index_t> &lengths, std::vector<terminal_t> &word) const;

};

#endif // GRAMMAR_H
