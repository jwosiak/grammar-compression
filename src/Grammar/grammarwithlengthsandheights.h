#ifndef GRAMMARWITHLENGTHSANDHEIGHTS_H
#define GRAMMARWITHLENGTHSANDHEIGHTS_H

#include "grammarwithlengths.h"
#include <cinttypes>
#include "types.h"

// contains length and height of each nonterminal

class GrammarWithLengthsAndHeights : public GrammarWithLengths
{
    std::vector<uint8_t> heights;
public:
    GrammarWithLengthsAndHeights();

    virtual void clearCache();

    uint8_t getHeight(index_t symbol) const;
    using Grammar::newNonterminal;
    virtual index_t newNonterminal(production_t &prod);

    virtual void setProd(index_t nonterminal, production_t &prod);
    virtual void setProd(index_t nonterminal, production_t &&prod);

    bool areBalanced(index_t leftProd, index_t rightProd) const;

private:
    void updateHeight(index_t nonterminal);

protected:
    virtual void resize(index_t nonterminalsNum);
};

#endif // GRAMMARWITHLENGTHSANDHEIGHTS_H
