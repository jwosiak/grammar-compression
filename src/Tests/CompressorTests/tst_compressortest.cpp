#include <QtTest>
#include <vector>
#include <functional>
#include <QString>

#include "types.h"
#include "gentools.h"

#include "grammarcompressor.h"
#include "recompressgrammarcompressor.h"
#include "avlbasedgrammarcompressor.h"
#include "alphabalancedgrammarcompressor.h"
#include "reallysimplegrammarcompressor.h"

typedef std::function<GrammarCompressor*(std::vector<terminal_t>&)>
    compressorConstr_t;

class CompressorTest : public QObject
{
    Q_OBJECT

public:
    CompressorTest();
    ~CompressorTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();
    void test_case2();
    void test_case3();

    friend class LazyGrammarConstructor;
private:
    std::vector<compressorConstr_t> constrs;
    void constructAndTestGrammars(const std::vector<terminal_t> &v);

};

CompressorTest::CompressorTest()
{
    auto c1 = [](std::vector<terminal_t> &v){
        return new RecompressGrammarCompressor(v); };
    constrs.push_back(c1);

    auto c2 = [](std::vector<terminal_t> &v){
        return new AvlBasedGrammarCompressor(v); };
    constrs.push_back(c2);

    auto c3 = [](std::vector<terminal_t> &v){
        return new AlphaBalancedGrammarCompressor(v, 0.292); };
    constrs.push_back(c3);

    auto c4 = [](std::vector<terminal_t> &v){
        return new ReallySimpleGrammarCompressor(v); };
    constrs.push_back(c4);
}

CompressorTest::~CompressorTest()
{

}

void CompressorTest::initTestCase()
{
    std::srand(std::time(0));
}

void CompressorTest::cleanupTestCase()
{

}

void CompressorTest::test_case1()
{
    for (int i = 5; i <= 50000; i *= 10)
        constructAndTestGrammars(Tools::randText(1, i));
}

void CompressorTest::test_case2()
{
    for (unsigned int i = 2; i < 23; i+=5)
        constructAndTestGrammars(Tools::fibonacciWord(i));
}

void CompressorTest::test_case3()
{
    for (int i = 5; i <= 250000; i *= 10)
        constructAndTestGrammars(Tools::randText(2, i));
}

void CompressorTest::constructAndTestGrammars(const std::vector<terminal_t> &v)
{
    int i = 0;
    for (compressorConstr_t constr : constrs) {
        std::vector<terminal_t> vCopy(v);
        qDebug() << "Start" << ++i;
        GrammarCompressor *compressor = constr(vCopy);
        compressor->constructGrammar();
        std::vector<terminal_t> compressorProduction =
                compressor->getGrammar().produceWord();
        QCOMPARE(v, compressorProduction);
        QCOMPARE(v.size(), compressorProduction.size());
        delete compressor;
        qDebug() << "End" << i;
    }
}

QTEST_APPLESS_MAIN(CompressorTest)

#include "tst_compressortest.moc"
