#include <QtTest>

#include "types.h"
#include "gentools.h"
#include "lzcompressor.h"
#include "factor.h"

typedef std::vector<Factor>
    (*factorizationFunction_t)(const std::vector<terminal_t>&);

class LZCompressionTest : public QObject
{
    Q_OBJECT

    const std::vector<factorizationFunction_t>
        allFactorizationFunctions = { LZCompressor::selfRef,
                                      LZCompressor::noSelfRef };
public:
    LZCompressionTest();
    ~LZCompressionTest();

private slots:
    void test_case1();
    void test_case2();
    void test_case3();
    void test_case4();
    void test_case5();
    void test_case6();
    void test_case7();
    void test_case8();
    void test_case9();

private:
    void testFactorizationProductionForWord(
            factorizationFunction_t func, const std::vector<terminal_t> &word);
    void testFactorizationSizeForWord(
            factorizationFunction_t func, const std::vector<terminal_t> &word);
    void testEachFactorizationProductionAndSizeForWord(
            const std::vector<terminal_t> &word);
    void testEachFactorizationProduction(
            const std::vector<terminal_t> &word);
};

LZCompressionTest::LZCompressionTest()
{
    std::srand(std::time(0));
}

LZCompressionTest::~LZCompressionTest()
{

}

void LZCompressionTest::test_case1()
{
    std::vector<std::vector<terminal_t>> inputs = {
        { 0, 0, 0 },
        { 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1 },
        { 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1},
        { 0, 1, 0, 2, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1},
        { 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
          0, 0, 0, 0, 0, 0, 0, 0},
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        {0, 1, 0, 0, 0, 1},
        {0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0},
        {0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0},
        {0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0},
        {3, 0, 1, 3, 1, 1, 3, 3, 0, 0, 1, 1, 1, 3, 1, 3, 1, 2, 1, 3},
        {1, 1, 2, 0, 1, 0, 1, 1, 1, 1, 1, 3, 0, 3},
        {0, 0, 1, 0, 0, 1, 0, 1, 0, 2},
        {0, 0, 0, 1, 0, 0, 1, 0, 1, 0},
        {0, 1, 0, 0, 1, 0, 0, 0, 0, 2},
        {0, 0, 1, 1, 1, 2, 1, 1, 2, 1, 2, 1, 1, 2, 1},
    };

    for (const std::vector<terminal_t> &word : inputs)
        testEachFactorizationProductionAndSizeForWord(word);
}

void LZCompressionTest::test_case2()
{
    for (index_t i = 2; i < 20; i+=5)
        testEachFactorizationProductionAndSizeForWord(Tools::fibonacciWord(i));
}

void LZCompressionTest::test_case3()
{
    for (index_t i = 5; i <= 5000; i *= 10)
        testEachFactorizationProductionAndSizeForWord(Tools::randText(1, i));
}

void LZCompressionTest::test_case4()
{
    for (index_t i = 5; i <= 5000; i *= 10)
        testEachFactorizationProductionAndSizeForWord(Tools::randText(2, i));
}

void LZCompressionTest::test_case5()
{
    for (index_t i = 5; i <= 2000000; i *= 10)
        testEachFactorizationProduction(Tools::randText(2, i));
}

void LZCompressionTest::test_case6()
{
    for (index_t i = 1; i <= 20; i += 4)
        testEachFactorizationProductionAndSizeForWord(
                    Tools::genTextWithManyBlocks(256*i, i));
}

void LZCompressionTest::test_case7()
{
    for (index_t i = 1; i <= 5000; i *= 2)
        testEachFactorizationProduction(
                    Tools::genTextWithManyBlocks(256*i, i));
}

void LZCompressionTest::test_case8()
{
    for (index_t i = 5; i <= 5000; i *= 10)
        testEachFactorizationProductionAndSizeForWord(
                    Tools::genIncreasingLengthBlocks(i));
}

void LZCompressionTest::test_case9()
{
    for (index_t i = 5; i <= 2000000; i *= 10)
        testEachFactorizationProduction(Tools::genIncreasingLengthBlocks(i));
}

void LZCompressionTest::testFactorizationProductionForWord(
        factorizationFunction_t func, const std::vector<terminal_t> &word)
{
    std::vector<Factor> factorizationToTest = func(word);
    QCOMPARE(word, LZCompressor::decode(factorizationToTest));
}

void LZCompressionTest::testFactorizationSizeForWord(
        factorizationFunction_t func, const std::vector<terminal_t> &word)
{
    std::vector<Factor> factorizationToTest = func(word);
    std::vector<Factor> correctNoSelfRef =
            LZCompressor::naiveNoSelfRef(word);
    QVERIFY(correctNoSelfRef.size() >= factorizationToTest.size());
}

void LZCompressionTest::testEachFactorizationProductionAndSizeForWord(
        const std::vector<terminal_t> &word)
{
    for (factorizationFunction_t factorizationFunction
         : allFactorizationFunctions) {
        testFactorizationProductionForWord(factorizationFunction, word);
        testFactorizationSizeForWord(factorizationFunction, word);
    }
}

void LZCompressionTest::testEachFactorizationProduction(
        const std::vector<terminal_t> &word)
{
    for (factorizationFunction_t factorizationFunction
         : allFactorizationFunctions)
        testFactorizationProductionForWord(factorizationFunction, word);
}

QTEST_APPLESS_MAIN(LZCompressionTest)

#include "tst_lzcompressiontest.moc"
