QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  \
    tst_lzcompressiontest.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/AlphaBalancedGrammarCompressor/release/ -lAlphaBalancedGrammarCompressor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/AlphaBalancedGrammarCompressor/debug/ -lAlphaBalancedGrammarCompressor
else:unix: LIBS += -L$$OUT_PWD/../../GrammarCompressor/AlphaBalancedGrammarCompressor/ -lAlphaBalancedGrammarCompressor

INCLUDEPATH += $$PWD/../../GrammarCompressor/AlphaBalancedGrammarCompressor
DEPENDPATH += $$PWD/../../GrammarCompressor/AlphaBalancedGrammarCompressor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/AlphaBalancedGrammarCompressor/release/libAlphaBalancedGrammarCompressor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/AlphaBalancedGrammarCompressor/debug/libAlphaBalancedGrammarCompressor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/AlphaBalancedGrammarCompressor/release/AlphaBalancedGrammarCompressor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/AlphaBalancedGrammarCompressor/debug/AlphaBalancedGrammarCompressor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/AlphaBalancedGrammarCompressor/libAlphaBalancedGrammarCompressor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/AvlBasedGrammarCompressor/release/ -lAvlBasedGrammarCompressor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/AvlBasedGrammarCompressor/debug/ -lAvlBasedGrammarCompressor
else:unix: LIBS += -L$$OUT_PWD/../../GrammarCompressor/AvlBasedGrammarCompressor/ -lAvlBasedGrammarCompressor

INCLUDEPATH += $$PWD/../../GrammarCompressor/AvlBasedGrammarCompressor
DEPENDPATH += $$PWD/../../GrammarCompressor/AvlBasedGrammarCompressor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/AvlBasedGrammarCompressor/release/libAvlBasedGrammarCompressor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/AvlBasedGrammarCompressor/debug/libAvlBasedGrammarCompressor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/AvlBasedGrammarCompressor/release/AvlBasedGrammarCompressor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/AvlBasedGrammarCompressor/debug/AvlBasedGrammarCompressor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/AvlBasedGrammarCompressor/libAvlBasedGrammarCompressor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/ReallySimpleGrammarCompressor/release/ -lReallySimpleGrammarCompressor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/ReallySimpleGrammarCompressor/debug/ -lReallySimpleGrammarCompressor
else:unix: LIBS += -L$$OUT_PWD/../../GrammarCompressor/ReallySimpleGrammarCompressor/ -lReallySimpleGrammarCompressor

INCLUDEPATH += $$PWD/../../GrammarCompressor/ReallySimpleGrammarCompressor
DEPENDPATH += $$PWD/../../GrammarCompressor/ReallySimpleGrammarCompressor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/ReallySimpleGrammarCompressor/release/libReallySimpleGrammarCompressor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/ReallySimpleGrammarCompressor/debug/libReallySimpleGrammarCompressor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/ReallySimpleGrammarCompressor/release/ReallySimpleGrammarCompressor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/ReallySimpleGrammarCompressor/debug/ReallySimpleGrammarCompressor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/ReallySimpleGrammarCompressor/libReallySimpleGrammarCompressor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/RecompressGrammarCompressor/release/ -lRecompressGrammarCompressor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/RecompressGrammarCompressor/debug/ -lRecompressGrammarCompressor
else:unix: LIBS += -L$$OUT_PWD/../../GrammarCompressor/RecompressGrammarCompressor/ -lRecompressGrammarCompressor

INCLUDEPATH += $$PWD/../../GrammarCompressor/RecompressGrammarCompressor
DEPENDPATH += $$PWD/../../GrammarCompressor/RecompressGrammarCompressor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/RecompressGrammarCompressor/release/libRecompressGrammarCompressor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/RecompressGrammarCompressor/debug/libRecompressGrammarCompressor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/RecompressGrammarCompressor/release/RecompressGrammarCompressor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/RecompressGrammarCompressor/debug/RecompressGrammarCompressor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/RecompressGrammarCompressor/libRecompressGrammarCompressor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/LZCompressor/release/ -lLZCompressor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/LZCompressor/debug/ -lLZCompressor
else:unix: LIBS += -L$$OUT_PWD/../../GrammarCompressor/LZCompressor/ -lLZCompressor

INCLUDEPATH += $$PWD/../../GrammarCompressor/LZCompressor
DEPENDPATH += $$PWD/../../GrammarCompressor/LZCompressor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/LZCompressor/release/libLZCompressor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/LZCompressor/debug/libLZCompressor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/LZCompressor/release/LZCompressor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/LZCompressor/debug/LZCompressor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/LZCompressor/libLZCompressor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/Interfaces/release/ -lInterfaces
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../GrammarCompressor/Interfaces/debug/ -lInterfaces
else:unix: LIBS += -L$$OUT_PWD/../../GrammarCompressor/Interfaces/ -lInterfaces

INCLUDEPATH += $$PWD/../../GrammarCompressor/Interfaces
DEPENDPATH += $$PWD/../../GrammarCompressor/Interfaces

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/Interfaces/release/libInterfaces.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/Interfaces/debug/libInterfaces.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/Interfaces/release/Interfaces.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/Interfaces/debug/Interfaces.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../GrammarCompressor/Interfaces/libInterfaces.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Tools/IOTools/release/ -lIOTools
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Tools/IOTools/debug/ -lIOTools
else:unix: LIBS += -L$$OUT_PWD/../../Tools/IOTools/ -lIOTools

INCLUDEPATH += $$PWD/../../Tools/IOTools
DEPENDPATH += $$PWD/../../Tools/IOTools

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/IOTools/release/libIOTools.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/IOTools/debug/libIOTools.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/IOTools/release/IOTools.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/IOTools/debug/IOTools.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../Tools/IOTools/libIOTools.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Tools/GenTools/release/ -lGenTools
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Tools/GenTools/debug/ -lGenTools
else:unix: LIBS += -L$$OUT_PWD/../../Tools/GenTools/ -lGenTools

INCLUDEPATH += $$PWD/../../Tools/GenTools
DEPENDPATH += $$PWD/../../Tools/GenTools

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/GenTools/release/libGenTools.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/GenTools/debug/libGenTools.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/GenTools/release/GenTools.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/GenTools/debug/GenTools.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../Tools/GenTools/libGenTools.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Grammar/release/ -lGrammar
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Grammar/debug/ -lGrammar
else:unix: LIBS += -L$$OUT_PWD/../../Grammar/ -lGrammar

INCLUDEPATH += $$PWD/../../Grammar
DEPENDPATH += $$PWD/../../Grammar

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Grammar/release/libGrammar.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Grammar/debug/libGrammar.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Grammar/release/Grammar.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Grammar/debug/Grammar.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../Grammar/libGrammar.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../Tools/Types/release/ -lTypes
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../Tools/Types/debug/ -lTypes
else:unix: LIBS += -L$$OUT_PWD/../../Tools/Types/ -lTypes

INCLUDEPATH += $$PWD/../../Tools/Types
DEPENDPATH += $$PWD/../../Tools/Types

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/Types/release/libTypes.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/Types/debug/libTypes.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/Types/release/Types.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../Tools/Types/debug/Types.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../Tools/Types/libTypes.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../SuffixArrayLib/release/ -lSuffixArrayLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../SuffixArrayLib/debug/ -lSuffixArrayLib
else:unix: LIBS += -L$$OUT_PWD/../../SuffixArrayLib/ -lSuffixArrayLib

INCLUDEPATH += $$PWD/../../SuffixArrayLib
DEPENDPATH += $$PWD/../../SuffixArrayLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../SuffixArrayLib/release/libSuffixArrayLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../SuffixArrayLib/debug/libSuffixArrayLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../SuffixArrayLib/release/SuffixArrayLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../SuffixArrayLib/debug/SuffixArrayLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../SuffixArrayLib/libSuffixArrayLib.a
