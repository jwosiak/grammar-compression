#include "commonprefixpositions.h"

CommonPrefixPositions::CommonPrefixPositions(
        index_t commonPrefixLength, index_t position)
    : commonPrefixLength(commonPrefixLength)
    , minPosition(position)
{
    positions.reserve(10);
    insertPosition(position);
}

CommonPrefixPositions &CommonPrefixPositions::insertPosition(index_t position)
{
    minPosition = std::min(minPosition, position);
    positions.push_back(position);
    return *this;
}
