#ifndef GRAMMARDECOMPOSEROFFACTOR_H
#define GRAMMARDECOMPOSEROFFACTOR_H

#include "types.h"

class GrammarWithLengths;
class GrammarDecomposition;
class Factor;

class GrammarDecomposerOfFactor
{
    const GrammarWithLengths &grammar;

public:
    GrammarDecomposerOfFactor(const GrammarWithLengths &grammar);

    GrammarDecomposition decomposeFactor(Factor factor);

    // finds the decomposition of the factor which is a substring of
    //  the nonterminal
    GrammarDecomposition decomposeInNonterminal(
            index_t nonterminal, Factor factor);

    // finds the decomposition of suffix of the nonterminal's production
    //   which starts in the production at the position
    GrammarDecomposition leftDecomposeInNonterminal(
            index_t nonterminal, index_t factorPositionInNonterminal);

    // finds the decomposition of prefix of the nonterminal's production
    GrammarDecomposition rightDecomposeInNonterminal(
            index_t nonterminal, index_t factorLengthInNonterminal);
private:

    // returns true if the nonterminal's production is equal to the factor
    bool nonterminalProducesFactor(index_t nonterminal, const Factor &factor);

    GrammarDecomposition getSingleNonterminalDecomposition(
            index_t nonterminal);

    GrammarDecomposition& assignLeftDecomposition(
            GrammarDecomposition &decomposition, index_t root,
            index_t factorPositionInRoot);
    GrammarDecomposition& assignRightDecomposition(
            GrammarDecomposition &decomposition, index_t root,
            index_t factorLengthInRoot);
};

#endif // GRAMMARDECOMPOSEROFFACTOR_H
