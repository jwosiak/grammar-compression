#ifndef COMMONPREFIXPOSITIONS_H
#define COMMONPREFIXPOSITIONS_H

#include "types.h"

// positions in the word which have a common prefix of length at least
//   commonPrefixLength

struct CommonPrefixPositions
{
    index_t commonPrefixLength;
    index_t minPosition;
    std::vector<index_t> positions;

    CommonPrefixPositions(index_t commonPrefixLength, index_t position);
    CommonPrefixPositions &insertPosition(index_t position);
};

#endif // COMMONPREFIXPOSITIONS_H
