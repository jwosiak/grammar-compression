#ifndef LONGESTPREVIOUSFACTORCOMPUTATOR_H
#define LONGESTPREVIOUSFACTORCOMPUTATOR_H

#include <vector>
#include "types.h"
#include "divsufsort.h"
#include "commonprefixpositions.h"

class Factor;

class LongestPreviousFactorComputator
{
    const std::vector<terminal_t> &word;

    saidx_t *suffixArray;
    std::vector<index_t> longestCommonPrefix;

    // aux collection for calculating factors
    std::vector<CommonPrefixPositions> prefixLevels;

    // output all factors collection
    std::vector<Factor> allFactors;
public:
    LongestPreviousFactorComputator(const std::vector<terminal_t> &word);

    std::vector<Factor> &computeSelfRefFactors();
    std::vector<Factor> &computeLeftmostSelfRefFactors();

private:
    saidx_t *computeSuffixArray();

    bool isPositionInRange(index_t posInWord);
    index_t getCommonPrefixLength(index_t posInWord1, index_t posInWord2);
    std::vector<index_t> &computeLongestCommonPrefix();

    void clearSAAndLCP();

    std::vector<Factor> &computeFactors();

    bool isNotSetFactorAtPos(index_t pos);
    std::vector<Factor> &assignFactorsFromLevel(
            CommonPrefixPositions &commonPrefixPositions);
    std::vector<CommonPrefixPositions> &removeAndProcessHigherPrefixLevelsThanCurrent(
            index_t currPosInWord, index_t currLevel);
    std::vector<CommonPrefixPositions> &pushNextHighestPrefixLevel(
            index_t prevPosInWord, index_t currPosInWord, index_t currLevel);
    std::vector<CommonPrefixPositions> &processPositionInSA(
            index_t positionInSA);
    std::vector<CommonPrefixPositions> &processRemainingHighestPrefixLevel();
    std::vector<Factor> &computeLeftmostFactors();
};

#endif // LONGESTPREVIOUSFACTORCOMPUTATOR_H
