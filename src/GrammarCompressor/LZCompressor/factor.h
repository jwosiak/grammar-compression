#ifndef FACTOR_H
#define FACTOR_H

#include "types.h"

struct Factor
{
    index_t position;
    index_t length;

    Factor();
    Factor(index_t freeLetter);
    Factor(index_t position, index_t length);

    index_t getEndPosition() const;
    bool isFreeLetter() const;

    // works properly only if the factor is a free letter
    index_t getFreeLetter() const;
};

#endif // FACTOR_H
