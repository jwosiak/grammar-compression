#include "grammardecomposeroffactor.h"
#include "grammarwithlengths.h"
#include "factor.h"
#include "grammardecomposition.h"


GrammarDecomposerOfFactor::GrammarDecomposerOfFactor(
        const GrammarWithLengths &grammar)
    : grammar(grammar)
{

}

GrammarDecomposition GrammarDecomposerOfFactor::decomposeFactor(Factor factor)
{
    return decomposeInNonterminal(grammar.getMaxSymbol(), factor);
}

GrammarDecomposition GrammarDecomposerOfFactor::decomposeInNonterminal(
        index_t nonterminal, Factor factor)
{
    if (nonterminalProducesFactor(nonterminal, factor))
        return getSingleNonterminalDecomposition(nonterminal);

    production_t prod = grammar.getProd(nonterminal);
    index_t leftLength = grammar.getLength(prod.first);

    // the factor is produced by nonterminals from the left subtree
    if (factor.getEndPosition() < leftLength)
        return decomposeInNonterminal(prod.first, factor);

    // the factor is produced by nonterminals from the right subtree
    if (factor.position >= leftLength) {
        factor.position -= leftLength;
        return decomposeInNonterminal(prod.second, factor);
    }

    // prefix of the factor is produced by the left subtree
    // suffix is produced by the right subtree
    index_t rightPartLength = factor.length
            - (leftLength - factor.position);

    GrammarDecomposition decomposition;
    decomposition = assignLeftDecomposition(decomposition,
                                            prod.first, factor.position);
    decomposition = assignRightDecomposition(decomposition,
                                             prod.second, rightPartLength);

    return decomposition;
}

GrammarDecomposition GrammarDecomposerOfFactor::leftDecomposeInNonterminal(
        index_t nonterminal, index_t factorPositionInNonterminal)
{
    GrammarDecomposition decomposition;
    decomposition = assignLeftDecomposition(decomposition, nonterminal,
                                            factorPositionInNonterminal);
    return decomposition;
}

GrammarDecomposition GrammarDecomposerOfFactor::rightDecomposeInNonterminal(
        index_t nonterminal, index_t factorLengthInNonterminal)
{
    GrammarDecomposition decomposition;
    decomposition = assignRightDecomposition(decomposition, nonterminal,
                                             factorLengthInNonterminal);
    return decomposition;
}

bool GrammarDecomposerOfFactor::nonterminalProducesFactor(
        index_t nonterminal, const Factor &factor)
{
    index_t nonterminalLength = grammar.getLength(nonterminal);
    return (factor.position == 0)
            && (factor.getEndPosition() == nonterminalLength - 1);
}

GrammarDecomposition
    GrammarDecomposerOfFactor::getSingleNonterminalDecomposition(
        index_t nonterminal)
{
    GrammarDecomposition decomposition;
    decomposition.pushLeft(nonterminal);
    return decomposition;
}

GrammarDecomposition &GrammarDecomposerOfFactor::assignLeftDecomposition(
        GrammarDecomposition &decomposition, index_t root,
        index_t factorPositionInRoot)
{
    // traverses down the tree
    // every time moving to left subtree, the right nonterminal
    //   (right subtree) must be added to the decomposition,
    //   because it produces a part of the suffix of root's production
    while (factorPositionInRoot > 0) {
        production_t prod = grammar.getProd(root);
        index_t leftLength = grammar.getLength(prod.first);

        if (factorPositionInRoot >= leftLength) {
            factorPositionInRoot -= leftLength;
            root = prod.second;
        } else {
            decomposition.pushLeft(prod.second);
            root = prod.first;
        }
    }
    decomposition.pushLeft(root);
    return decomposition;
}

GrammarDecomposition &GrammarDecomposerOfFactor::assignRightDecomposition(
        GrammarDecomposition &decomposition, index_t root,
        index_t factorLengthInRoot)
{
    // traverses down the tree
    // every time moving to right subtree, the left nonterminal
    //   (left subtree) must be added to the decomposition,
    //   because it produces a part of the prefix of root's production
    index_t rootLength = grammar.getLength(root);
    while (rootLength != factorLengthInRoot) {
        production_t prod = grammar.getProd(root);
        index_t leftLength = grammar.getLength(prod.first);

        if (factorLengthInRoot <= leftLength) {
            root = prod.first;
            rootLength = leftLength;
        } else {
            decomposition.pushRight(prod.first);
            factorLengthInRoot -= leftLength;
            root = prod.second;
            rootLength -= leftLength;
        }
    }
    decomposition.pushRight(root);
    return decomposition;
}
