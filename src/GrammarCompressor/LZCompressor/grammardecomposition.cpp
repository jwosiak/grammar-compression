#include "grammardecomposition.h"

std::forward_list<index_t>& GrammarDecomposition::getLeftDecomposition()
{
    return leftDecomposition;
}

std::forward_list<index_t> &GrammarDecomposition::getRightDecomposition()
{
    return rightDecomposition;
}

GrammarDecomposition::GrammarDecomposition()
{

}

void GrammarDecomposition::pushLeft(index_t nonterminal)
{
    leftDecomposition.push_front(nonterminal);
}

void GrammarDecomposition::pushRight(index_t nonterminal)
{
    rightDecomposition.push_front(nonterminal);
}

bool GrammarDecomposition::isBitonic()
{
    return !rightDecomposition.empty();
}
