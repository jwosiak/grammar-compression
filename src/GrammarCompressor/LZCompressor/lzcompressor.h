#ifndef LZCOMPRESSOR_H
#define LZCOMPRESSOR_H

#include <vector>
#include "types.h"

class Factor;

namespace LZCompressor
{
    // self ref in the context of factors means that self referencing
    //   factors may occurr, not that all factors are self referencing
    // no self ref means that there isn't any self referencing factor

    std::vector<Factor> selfRef(const std::vector<terminal_t> &word);
    std::vector<Factor> noSelfRef(const std::vector<terminal_t> &word);
    std::vector<Factor> naiveNoSelfRef(const std::vector<terminal_t> &word);
    std::vector<terminal_t> decode(const std::vector<Factor> &factorization);
};

#endif // LZCOMPRESSOR_H
