#include "factor.h"


Factor::Factor()
    : position(0)
    , length(0)
{

}

Factor::Factor(index_t freeLetter)
    // the free letter is kept in position field
    : position(freeLetter)
    , length(1)
{

}

Factor::Factor(index_t position, index_t length)
    : position(position)
    , length(length)
{

}

index_t Factor::getEndPosition() const
{
    return position + length - 1;
}

bool Factor::isFreeLetter() const
{
    return length <= 1;
}

index_t Factor::getFreeLetter() const
{
    // the free letter is kept in position field
    return position;
}
