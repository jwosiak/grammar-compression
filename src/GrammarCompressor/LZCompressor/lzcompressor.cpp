﻿#include "lzcompressor.h"
#include "longestpreviousfactorcomputator.h"
#include "factor.h"

using namespace LZCompressor;

std::vector<Factor> LZCompressor::selfRef(
        const std::vector<terminal_t> &word)
{
    LongestPreviousFactorComputator computator(word);
    std::vector<Factor> &allFactors = computator.computeSelfRefFactors();
    // factorization is a subset of all factors
    // these factors produce the word
    std::vector<Factor> factorization;

    for (index_t i = 0; i < word.size();) {
        Factor &factor = allFactors[i];
        if (factor.length <= 1)
            factorization.push_back(Factor(word[i]));
        else
            factorization.push_back(factor);

        i += std::max(index_t(1), factor.length);
    }

    return factorization;
}

std::vector<Factor> LZCompressor::noSelfRef(
        const std::vector<terminal_t> &word)
{
    LongestPreviousFactorComputator computator(word);
    std::vector<Factor> &allFactors =
            computator.computeLeftmostSelfRefFactors();
    // factorization is a subset of all factors
    // these factors produce the word
    std::vector<Factor> factorization;

    for (index_t i = 0; i < word.size();) {
        Factor &factor = allFactors[i];
        if (factor.getEndPosition() >= i) {
            index_t boundedLpf = std::min(factor.length, i - factor.position);
            Factor &factorOfFactor = allFactors[factor.position];
            if (factorOfFactor.length > boundedLpf)
                factor = factorOfFactor;
            else
                factor.length = boundedLpf;
        }

        if (factor.isFreeLetter())
            factorization.push_back(Factor(word[i]));
        else
            factorization.push_back(Factor(factor.position, factor.length));

        i += std::max(index_t(1), factor.length);
    }

    return factorization;
}

std::vector<Factor> LZCompressor::naiveNoSelfRef(
        const std::vector<terminal_t> &word)
{
    std::vector<Factor> factorization;
    for (index_t factorOccurrencePos = 0; factorOccurrencePos < word.size(); ) {
        Factor longestFactor(word[factorOccurrencePos]);
        for (index_t factorDefinitionPos = 0
             ; factorDefinitionPos < factorOccurrencePos
             ; factorDefinitionPos++) {
            index_t factorDefinitionIter = factorDefinitionPos;
            index_t factorOccurrenceIter = factorOccurrencePos;
            while ((factorOccurrenceIter < word.size())
                   && (factorDefinitionIter < factorOccurrencePos)
                   && (word[factorOccurrenceIter]
                       == word[factorDefinitionIter])) {
                factorOccurrenceIter++;
                factorDefinitionIter++;
            }
            index_t currFactorLength =
                    factorDefinitionIter - factorDefinitionPos;
            if (currFactorLength > longestFactor.length)
                longestFactor = Factor(factorDefinitionPos, currFactorLength);
        }
        factorization.push_back(longestFactor);
        factorOccurrencePos += std::max(index_t(1), longestFactor.length);
    }
    return factorization;
}

std::vector<terminal_t> LZCompressor::decode(
        const std::vector<Factor> &factorization)
{
    std::vector<terminal_t> output;
    output.reserve(factorization.size());
    for (const Factor &factor : factorization) {
        if (factor.isFreeLetter())
            output.push_back(factor.getFreeLetter());
        else
            for (index_t i = 0; i < factor.length; i++)
                output.push_back(output[factor.position + i]);
    }
    return output;
}
