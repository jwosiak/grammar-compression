#ifndef GRAMMARDECOMPOSITION_H
#define GRAMMARDECOMPOSITION_H

#include "types.h"
#include <forward_list>

// class to decompose a substring of production of grammar's root symbol
//   as a sequence of nonterminals

class GrammarDecomposition
{
    // it represents the decomposition of substring's prefix
    std::forward_list<index_t> leftDecomposition;
    // it represents the decomposition of substring's suffix
    std::forward_list<index_t> rightDecomposition;

    // when a decomposition of substring is bitonic, then we have
    //   separate decompositions of prefix and suffic
    // when it's not, then we have only decomposition of the prefix
    //   (which is equal to the substring)

public:
    GrammarDecomposition();

    void pushLeft(index_t nonterminal);
    void pushRight(index_t nonterminal);
    bool isBitonic();
    std::forward_list<index_t>& getLeftDecomposition();
    std::forward_list<index_t>& getRightDecomposition();
};

#endif // GRAMMARDECOMPOSITION_H
