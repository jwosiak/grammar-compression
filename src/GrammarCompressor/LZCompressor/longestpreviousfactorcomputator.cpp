#include "longestpreviousfactorcomputator.h"
#include "factor.h"
#include <stack>

LongestPreviousFactorComputator::LongestPreviousFactorComputator(
        const std::vector<terminal_t> &word)
    : word(word)
{

}

std::vector<Factor> &LongestPreviousFactorComputator::computeSelfRefFactors()
{
    suffixArray = computeSuffixArray();
    longestCommonPrefix = computeLongestCommonPrefix();
    allFactors = computeFactors();
    clearSAAndLCP();
    return allFactors;
}

std::vector<Factor>
    &LongestPreviousFactorComputator::computeLeftmostSelfRefFactors()
{
    suffixArray = computeSuffixArray();
    longestCommonPrefix = computeLongestCommonPrefix();
    allFactors = computeLeftmostFactors();
    clearSAAndLCP();
    return allFactors;
}

saidx_t *LongestPreviousFactorComputator::computeSuffixArray()
{
    const sauchar_t *wordAdaptedToInterface = word.data();

    suffixArray = new saidx_t[word.size()+1];
    divsufsort(wordAdaptedToInterface, suffixArray, word.size());

    return suffixArray;
}

bool LongestPreviousFactorComputator::isPositionInRange(index_t posInWord)
{
    return posInWord < word.size();
}

index_t LongestPreviousFactorComputator::getCommonPrefixLength(
        index_t posInWord1, index_t posInWord2)
{
    index_t wordIter1 = posInWord1;
    index_t wordIter2 = posInWord2;
    index_t &rightmostWordIter = wordIter1 > wordIter2? wordIter1 : wordIter2;

    while (isPositionInRange(rightmostWordIter)
           && (word[wordIter1] == word[wordIter2])) {
        wordIter1++;
        wordIter2++;
    }

    index_t commonPrefixLength = wordIter1 - posInWord1;
    return commonPrefixLength;
}

std::vector<index_t>
    &LongestPreviousFactorComputator::computeLongestCommonPrefix()
{
    longestCommonPrefix.resize(word.size()+1);

    // rank and suffixArray[previous position] are kept together
    //   to avoid future random queries on suffixArray
    std::vector<indexpair_t> rankAndPrevSAElement(word.size());
    for (index_t i = 0; i < word.size(); i++) {
        index_t prevSAElement = i > 0? suffixArray[i-1] : 0;
        rankAndPrevSAElement[suffixArray[i]] =
                std::make_pair(i, prevSAElement);
    }

    index_t currentCommonPrefixLength = 0;
    for (index_t posInWord = 0; posInWord < word.size(); posInWord++) {
        indexpair_t posRankAndPrevPosSAElement =
                rankAndPrevSAElement[posInWord];
        index_t posInSA = posRankAndPrevPosSAElement.first;
        if (posInSA > 0) {
            // there is a common prefix at posInWord
            //   and at suffixArray[posInSA-1]
            index_t posWithCommonPrefix = posRankAndPrevPosSAElement.second;

            // there has been already found a common prefix of length at least
            //   currentCommonPrefixLength, and possibly there is a longer one
            currentCommonPrefixLength += getCommonPrefixLength(
                        posInWord + currentCommonPrefixLength,
                        posWithCommonPrefix + currentCommonPrefixLength);

            longestCommonPrefix[posInSA] = currentCommonPrefixLength;
            if (currentCommonPrefixLength > 0)
                currentCommonPrefixLength--;
        }
    }

    return longestCommonPrefix;
}

void LongestPreviousFactorComputator::clearSAAndLCP()
{
    delete[] suffixArray;
    longestCommonPrefix.clear();
}

std::vector<Factor> &LongestPreviousFactorComputator::computeFactors()
{
    allFactors.resize(word.size());

//    The algorithm below comes from the paper:
//    Maxime Crochemore, Lucian Ilie.
//    Computing Longest Previous Factor in linear time and applications.
//    Information Processing Letters, Elsevier, 2008, 106 (2),
//      pp.75-80. <10.1016/j.ipl.2007.10.006>. <hal-00619691>

    // a pair fields:
    // first - 'len'
    // second - 'pos'
    std::stack<std::pair<index_t, saidx_t>> stack;

    // there has been allocated additional space for (n+1)-th element
    suffixArray[word.size()] = -1;
    longestCommonPrefix[word.size()] = 0;

    stack.push(indexpair_t(0, suffixArray[0]));

    for (index_t i = 1; i <= word.size(); i++) {
        index_t lcp = longestCommonPrefix[i];
        while (!stack.empty() && (suffixArray[i] < stack.top().second)){
            index_t factorLength = std::max(stack.top().first, lcp);
            lcp = std::min(stack.top().first, lcp);
            indexpair_t v = stack.top();
            stack.pop();
            index_t factorPosition;
            if (v.first > lcp) factorPosition = stack.top().second;
            else factorPosition = suffixArray[i];
            allFactors[v.second] = Factor(factorPosition, factorLength);
        }
        if (i < word.size())
            stack.push(std::make_pair(lcp, suffixArray[i]));
    }

    return allFactors;
}

bool LongestPreviousFactorComputator::isNotSetFactorAtPos(index_t pos)
{
    return allFactors[pos].length == 0;
}

std::vector<Factor> &LongestPreviousFactorComputator::assignFactorsFromLevel(
        CommonPrefixPositions &commonPrefixPositions)
{
    for (index_t i = 0; i < commonPrefixPositions.positions.size(); i++) {
        index_t positionInWord = commonPrefixPositions.positions[i];
        if ((positionInWord > commonPrefixPositions.minPosition)
                && isNotSetFactorAtPos(positionInWord)) {
            allFactors[positionInWord] =
                    Factor(commonPrefixPositions.minPosition,
                           commonPrefixPositions.commonPrefixLength);
        }
    }
    return allFactors;
}

std::vector<CommonPrefixPositions>
    &LongestPreviousFactorComputator::removeAndProcessHigherPrefixLevelsThanCurrent(
        index_t currPosInWord, index_t currLevel)
{
    CommonPrefixPositions *highestPrefixLevel = &prefixLevels.back();
    allFactors = assignFactorsFromLevel(*highestPrefixLevel);
    index_t minPosition = highestPrefixLevel->minPosition;
    prefixLevels.pop_back();
    highestPrefixLevel = &prefixLevels.back();

    if (currLevel > highestPrefixLevel->commonPrefixLength) {
        prefixLevels
                .push_back(CommonPrefixPositions(currLevel, currPosInWord)
                           .insertPosition(minPosition));
    } else {
        highestPrefixLevel->insertPosition(minPosition);
        if(currLevel < highestPrefixLevel->commonPrefixLength)
            prefixLevels =
                removeAndProcessHigherPrefixLevelsThanCurrent(currPosInWord,
                                                              currLevel);
        else // case: currLevel == highestPrefixLevel->commonPrefixLength
            highestPrefixLevel->insertPosition(currPosInWord);
    }

    return prefixLevels;
}

std::vector<CommonPrefixPositions>
    &LongestPreviousFactorComputator::pushNextHighestPrefixLevel(
        index_t prevPosInWord, index_t currPosInWord, index_t currLevel)
{
    prefixLevels.push_back(
                CommonPrefixPositions(currLevel, prevPosInWord)
                .insertPosition(currPosInWord));
    return prefixLevels;
}

std::vector<CommonPrefixPositions>
    &LongestPreviousFactorComputator::processPositionInSA(index_t positionInSA)
{
    CommonPrefixPositions &highestPrefixLevel = prefixLevels.back();
    index_t prevPosInWord = suffixArray[positionInSA-1];
    index_t currPosInWord = suffixArray[positionInSA];
    // length of the longest common prefix of prevPosInWord and currPosInWord
    index_t currLevel = longestCommonPrefix[positionInSA];

    if (currLevel < highestPrefixLevel.commonPrefixLength)
        prefixLevels =
                removeAndProcessHigherPrefixLevelsThanCurrent(currPosInWord,
                                                              currLevel);
    else if(currLevel > highestPrefixLevel.commonPrefixLength)
        prefixLevels =
                pushNextHighestPrefixLevel(prevPosInWord, currPosInWord,
                                           currLevel);
    else
        highestPrefixLevel.insertPosition(currPosInWord);

    return prefixLevels;
}

std::vector<CommonPrefixPositions>
    &LongestPreviousFactorComputator::processRemainingHighestPrefixLevel()
{
    CommonPrefixPositions &highestPrefixLevel = prefixLevels.back();
    index_t minPosition = highestPrefixLevel.minPosition;
    allFactors = assignFactorsFromLevel(highestPrefixLevel);
    prefixLevels.pop_back();

    if (!prefixLevels.empty()) {
        CommonPrefixPositions &nextHighestPrefixLevel = prefixLevels.back();
        nextHighestPrefixLevel.insertPosition(minPosition);
    }

    return prefixLevels;
}

std::vector<Factor> &LongestPreviousFactorComputator::computeLeftmostFactors()
{
    allFactors.resize(word.size());

    prefixLevels.push_back(CommonPrefixPositions(0, suffixArray[0]));

    for (index_t posInSA = 1; posInSA < word.size(); posInSA++)
        prefixLevels = processPositionInSA(posInSA);

    while (!prefixLevels.empty())
        prefixLevels = processRemainingHighestPrefixLevel();

    return allFactors;
}
