#include "alphabalancedgrammarcompressor.h"
#include "lzcompressor.h"
#include "grammardecomposition.h"
#include "factor.h"
#include "activelist.h"
#include "activelistqueryresult.h"

AlphaBalancedGrammarCompressor::AlphaBalancedGrammarCompressor(
        std::vector<terminal_t> &word, float alpha)
    : word(word)
    , factorDecomposer(GrammarDecomposerOfFactor(grammar))
    , leftAddPairOperation(AddPairOperation(TreeOperation::LEFT,
                                            alpha, grammar))
    , rightAddPairOperation(AddPairOperation(TreeOperation::RIGHT,
                                             alpha, grammar))
{

}

void AlphaBalancedGrammarCompressor::constructGrammar()
{
    std::vector<Factor> lz = LZCompressor::noSelfRef(word);
    word.clear();
    ActiveList activeList(grammar);

    for (Factor &factor : lz) {
        index_t factorRoot = factor.isFreeLetter()
                ? factor.getFreeLetter()
                  // modifies activeList - a sequence of active nonterminals
                  //   may be replaced with a single nonterminal
                : constructNonterminalProducingFactor(activeList, factor);

        activeList.append(factorRoot);
    }

    std::vector<index_t> activeNonterminals =
            activeList.getActiveNonterminals();
    // side effect: the result of addSequence becomes
    //   the root symbol of grammar
    addSequence(activeNonterminals);
    // some nonterminals are unused due to replacements of
    //   sequences A_1, A_2, ... , A_t in the active list
    grammar.removeUnusedNonterminals();
}

Grammar &AlphaBalancedGrammarCompressor::getGrammar()
{
    return grammar;
}

index_t AlphaBalancedGrammarCompressor::merge(
        index_t leftNonterminal, index_t rightNonterminal)
{
    return addPair(leftNonterminal, rightNonterminal);
}

index_t AlphaBalancedGrammarCompressor::addPair(
        index_t leftNonterminal, index_t rightNonterminal)
{
    if (grammar.getLength(leftNonterminal)
            >= grammar.getLength(rightNonterminal))
        return rightAddPairOperation
                .perform(leftNonterminal, rightNonterminal);

    return leftAddPairOperation.perform(leftNonterminal, rightNonterminal);
}

index_t AlphaBalancedGrammarCompressor::addSequence(
        std::vector<index_t> &toAdd)
{
    // invokes addPair on neighbouring nonterminals
    //   until there's only one nonterminal
    while (toAdd.size() > 1) {
        index_t mod2 = toAdd.size() % 2;
        for (index_t i = 0; i < toAdd.size() - mod2; i += 2) {
            index_t mergedRule = addPair(toAdd[i], toAdd[i+1]);
            toAdd[i / 2] = mergedRule;
        }
        if (mod2 == 1)
            // unpaired nonterminal
            toAdd[toAdd.size() / 2] = toAdd.back();
        toAdd.resize((toAdd.size() / 2) + mod2);
    }
    return toAdd.front();
}

index_t AlphaBalancedGrammarCompressor::addSequence(
        index_t firstNonterminal, index_t secondNonterminal,
        index_t thirdNonterminal)
{
    std::vector<index_t> toAdd = { firstNonterminal, secondNonterminal,
                                   thirdNonterminal };
    return addSequence(toAdd);
}

index_t AlphaBalancedGrammarCompressor::constructXPrim(
        const ActiveListQueryResult &factorActiveForm)
{
    // addSubstring operation; X contains a prefix of the factor
    GrammarDecomposition XPrimDecomposition =
            factorDecomposer.leftDecomposeInNonterminal(
                factorActiveForm.X, factorActiveForm.posOfFactorInX);
    index_t XPrim = mergeDecomposition(XPrimDecomposition);
    return XPrim;
}

index_t AlphaBalancedGrammarCompressor::constructYPrim(
        const ActiveListQueryResult &factorActiveForm)
{
    // addSubstring operation; Y contains a suffix of the factor
    GrammarDecomposition YPrimDecomposition =
            factorDecomposer.rightDecomposeInNonterminal(
                factorActiveForm.Y, factorActiveForm.lengthOfFactorSuffixInY);
    index_t YPrim = mergeOneSidedDecomposition(YPrimDecomposition, false);
    return YPrim;
}

index_t AlphaBalancedGrammarCompressor::constructNonterminalProducingFactor(
        ActiveList &activeList, Factor &factor)
{
    ActiveListQueryResult factorActiveForm =
            activeList.getFactorRepresentation(factor);

    if (factorActiveForm.XContainsWholeFactor) {
        // addSubstring operation; the factor is completely contained in X
        factor.position = factorActiveForm.posOfFactorInX;
        GrammarDecomposition factorDecompositionInGrammar = factorDecomposer
                .decomposeInNonterminal(factorActiveForm.X, factor);
        // merging methods are the interface of the base class;
        //   in this class they are based on addPair operation
        return mergeDecomposition(factorDecompositionInGrammar);
    }

    // the factor has prefix in X, suffix in Y, and mid in A_1, A_2, ... , A_t

    index_t XPrim = constructXPrim(factorActiveForm);
    index_t YPrim = constructYPrim(factorActiveForm);
    index_t N;

    if (factorActiveForm.ASequence.size() > 0) {
        index_t M = addSequence(factorActiveForm.ASequence);
        activeList.replaceSequenceOfNonterminalsByNonterminal(
                    factorActiveForm.ASequenceBeginIterInActiveList,
                    factorActiveForm.ASequenceEndIterInActiveList,
                    M);
        N = addSequence(XPrim, M, YPrim);
    } else {
        N = addPair(XPrim, YPrim);
    }
    return N;
}
