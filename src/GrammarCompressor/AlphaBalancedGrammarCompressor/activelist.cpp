#include "activelist.h"
#include "grammarwithlengths.h"
#include "factor.h"
#include "activelistqueryresult.h"

ActiveList::ActiveList(const GrammarWithLengths &grammar)
    : grammar(grammar)
    , posInWordAfterLastActiveNonterminal(0)
{

}

ActiveListQueryResult ActiveList::getFactorRepresentation(const Factor &factor)
{
    std::map<index_t, index_t>::iterator firstActNontContainingFactor =
            getFirstActNontContainingPos(factor.position);
    std::map<index_t, index_t>::iterator lastActNontContainingFactor =
            getFirstActNontContainingPos(factor.getEndPosition());

    index_t firstActNontPos = firstActNontContainingFactor->first;
    index_t lastActNontPos = lastActNontContainingFactor->first;
//    index_t actNontsCount = lastActNontPos - firstActNontPos + 1;

    ActiveListQueryResult result;
    result.X = firstActNontContainingFactor->second;
    result.posOfFactorInX = factor.position - firstActNontPos;
    if (firstActNontPos == lastActNontPos) {
        result.XContainsWholeFactor = true;
    } else {
        result.XContainsWholeFactor = false;

        auto beginIterOfASequence = ++firstActNontContainingFactor;
        auto endIterOfASequence = lastActNontContainingFactor;

        result.ASequence = copyActiveNonterminals(beginIterOfASequence,
                                                  endIterOfASequence);

        result.ASequenceBeginIterInActiveList = beginIterOfASequence;
        result.ASequenceEndIterInActiveList = endIterOfASequence;

        result.Y = posToActiveNonterminal[lastActNontPos];
        result.lengthOfFactorSuffixInY =
                factor.getEndPosition() - lastActNontPos + 1;
    }

    return result;
}

void ActiveList::append(index_t nonterminal)
{
    posToActiveNonterminal[posInWordAfterLastActiveNonterminal] = nonterminal;
    posInWordAfterLastActiveNonterminal += grammar.getLength(nonterminal);
}

void ActiveList::replaceSequenceOfNonterminalsByNonterminal(
        std::map<index_t, index_t>::iterator beginIterInActNonts,
        std::map<index_t, index_t>::iterator endIterInActNonts,
        index_t replacingNonterminal)
{
    beginIterInActNonts->second = replacingNonterminal;
    posToActiveNonterminal.erase(++beginIterInActNonts, endIterInActNonts);
}

std::vector<index_t> ActiveList::getActiveNonterminals()
{
    return copyActiveNonterminals(posToActiveNonterminal.begin(),
                                  posToActiveNonterminal.end());
}

std::map<index_t, index_t>::iterator
    ActiveList::getFirstActNontContainingPos(index_t textPos)
{
    std::map<index_t, index_t>::iterator actNontIt =
            posToActiveNonterminal.lower_bound(textPos);

    index_t posOfActNontNotBeforeTextPos = actNontIt->first;
    if ((posOfActNontNotBeforeTextPos > textPos)
            || (actNontIt == posToActiveNonterminal.end()))
        actNontIt--;

    return actNontIt;
}

std::vector<index_t> ActiveList::copyActiveNonterminals(
        std::map<index_t, index_t>::iterator begin,
        std::map<index_t, index_t>::iterator end)
{
    std::vector<index_t> result;
    for (auto it = begin; it != end; it++)
        result.push_back(it->second);
    return result;
}
