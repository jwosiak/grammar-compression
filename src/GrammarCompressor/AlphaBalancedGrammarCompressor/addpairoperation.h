#ifndef ADDPAIROPERATION_H
#define ADDPAIROPERATION_H

#include "treeoperation.h"
#include "types.h"
#include <vector>

class GrammarWithLengths;

class AddPairOperation : TreeOperation
{
    GrammarWithLengths &grammar;

    const float lowerAlphaBound;

    // cached info; it is used to find out whether a nonterminal can be reused
    index_t maxSymbolAtBeginningOfOperation;
public:
    AddPairOperation(OperationType operationType, float alpha,
                     GrammarWithLengths &grammar);

    index_t perform(index_t X, index_t Y);

protected:
    Grammar &getGrammar();

    bool checkLowerAlphaBound(
            index_t numeratorNonterminal, index_t denominatorNonterminal);
    bool checkLowerAlphaBound(
            index_t numeratorNonterminal, production_t denominatorProd);

    std::vector<index_t> decomposeNonterminalUntilBalancedWithOther(
            index_t nonterminalToDecompose, index_t shorterNonterminal);
    index_t nextActiveRuleCase2(index_t Z_i, index_t A_i, production_t T_iProd);
    index_t nextActiveRuleCase3(
            index_t Z_i, index_t Y_iPlus1, index_t A_i, index_t B_i);
    index_t nextActiveRuleCase2Or3(index_t Z_i, index_t Y_iPlus1);
};

#endif // ADDPAIROPERATION_H
