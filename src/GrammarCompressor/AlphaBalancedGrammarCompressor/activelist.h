#ifndef ACTIVELIST_H
#define ACTIVELIST_H

#include <map>
#include "types.h"

class GrammarWithLengths;
class Factor;
class ActiveListQueryResult;

class ActiveList
{
public:
    // a position in the word -> an active nonterminal
    std::map<index_t, index_t> posToActiveNonterminal;
    index_t posInWordAfterLastActiveNonterminal;

    const GrammarWithLengths &grammar;

public:
    ActiveList(const GrammarWithLengths &grammar);

    ActiveListQueryResult getFactorRepresentation(
            const Factor &factor);

    void append(index_t nonterminal);

    void replaceSequenceOfNonterminalsByNonterminal(
            std::map<index_t, index_t>::iterator beginIterInActNonts,
            std::map<index_t, index_t>::iterator endIterInActNonts,
            index_t replacingNonterminal);

    std::vector<index_t> getActiveNonterminals();
private:
    std::map<index_t, index_t>::iterator
        getFirstActNontContainingPos(index_t textPos);

    std::vector<index_t> copyActiveNonterminals(
            std::map<index_t, index_t>::iterator begin,
            std::map<index_t, index_t>::iterator end);
};

#endif // ACTIVELIST_H
