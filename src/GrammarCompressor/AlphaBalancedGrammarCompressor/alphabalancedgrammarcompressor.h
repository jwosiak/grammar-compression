#ifndef ALPHABALANCEDGRAMMARCOMPRESSOR_H
#define ALPHABALANCEDGRAMMARCOMPRESSOR_H

#include "treemerginggrammarcompressor.h"
#include "types.h"
#include "grammarwithlengths.h"
#include "addpairoperation.h"
#include "grammardecomposeroffactor.h"

class ActiveList;
class ActiveListQueryResult;

class AlphaBalancedGrammarCompressor : public TreeMergingGrammarCompressor
{
private:
    std::vector<terminal_t> &word;
    GrammarWithLengths grammar;

    GrammarDecomposerOfFactor factorDecomposer;
    AddPairOperation leftAddPairOperation;
    AddPairOperation rightAddPairOperation;
public:
    AlphaBalancedGrammarCompressor(std::vector<terminal_t> &word, float alpha);

    // GrammarCompressor interface
    virtual void constructGrammar();
    virtual Grammar &getGrammar();

    // TreeMergingCompressor interface
protected:
    index_t merge(index_t leftNonterminal, index_t rightNonterminal);

private:
    bool checkLowerAlphaBound(
            index_t numeratorNonterminal, index_t denominatorNonterminal);
    bool checkUpperAlphaBound(
            index_t numeratorSymbol, index_t denominatorSymbol);

    index_t addPair(index_t leftNonterminal, index_t rightNonterminal);
    index_t addSequence(std::vector<index_t> &toAdd);
    index_t addSequence(index_t firstNonterminal, index_t secondNonterminal,
                        index_t thirdNonterminal);

    index_t constructXPrim(const ActiveListQueryResult &factorActiveForm);
    index_t constructYPrim(const ActiveListQueryResult &factorActiveForm);

    index_t constructNonterminalProducingFactor(
            ActiveList &activeList, Factor &factor);
};

#endif // ALPHABALANCEDGRAMMARCOMPRESSOR_H
