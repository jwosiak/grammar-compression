#include "addpairoperation.h"
#include "grammarwithlengths.h"

AddPairOperation::AddPairOperation(
        OperationType operationType, float alpha,
        GrammarWithLengths &grammar)
    : TreeOperation (TreeOperation::LEFT, operationType)
    , grammar(grammar)
    , lowerAlphaBound(alpha / (1 - alpha))
{

}

index_t AddPairOperation::perform(index_t X, index_t Y)
{
    maxSymbolAtBeginningOfOperation = grammar.getMaxSymbol();

    swapNonterminalsIfNotBaseCase(X, Y);

    std::vector<index_t> YDecomposition =
            decomposeNonterminalUntilBalancedWithOther(Y, X);

    // Z_1 = XY_1
    index_t Z_i = adjustedNewNonterminal(X, YDecomposition.back());
    YDecomposition.pop_back();

    while (!YDecomposition.empty()) {
        index_t Y_iPlus1 = YDecomposition.back();
        YDecomposition.pop_back();

        index_t Z_iPlus1 = checkLowerAlphaBound(Y_iPlus1, Z_i)
                ? adjustedNewNonterminal(Z_i, Y_iPlus1) // case 1
                : nextActiveRuleCase2Or3(Z_i, Y_iPlus1);

        Z_i = Z_iPlus1;
    }

    return Z_i;
}

Grammar &AddPairOperation::getGrammar()
{
    return grammar;
}

bool AddPairOperation::checkLowerAlphaBound(
        index_t numeratorNonterminal, index_t denominatorNonterminal)
{
    return lowerAlphaBound <= (float(grammar.getLength(numeratorNonterminal))
                               / grammar.getLength(denominatorNonterminal));
}

bool AddPairOperation::checkLowerAlphaBound(
        index_t numeratorNonterminal, production_t denominatorProd)
{
    float denominatorProdLength = grammar.getLength(denominatorProd.first)
            + grammar.getLength(denominatorProd.second);
    return lowerAlphaBound <= (grammar.getLength(numeratorNonterminal)
                               / denominatorProdLength);
}

std::vector<index_t>
    AddPairOperation::decomposeNonterminalUntilBalancedWithOther(
        index_t nonterminalToDecompose, index_t shorterNonterminal)
{
    std::vector<index_t> decomposition;
    decomposition.push_back(nonterminalToDecompose);

    index_t topNonterminal = nonterminalToDecompose;
    while (!checkLowerAlphaBound(shorterNonterminal, topNonterminal)) {
        if (topNonterminal > grammar.getMaxTerminal()) {
            production_t topNonterminalProd = grammar.getProd(topNonterminal);
            topNonterminalProd =
                    adjustProductionToOperationType(topNonterminalProd);
            decomposition.back() = topNonterminalProd.second;
            decomposition.push_back(topNonterminalProd.first);
        }
        topNonterminal = decomposition.back();
    }

    return decomposition;
}

index_t AddPairOperation::nextActiveRuleCase2(
        index_t Z_i, index_t A_i, production_t T_iProd)
{
    // now T_i is the highest nonterminal in the grammar
    index_t T_i = grammar.newNonterminal(T_iProd);
    // set Z_i should be higher than T_i, because it will become Z_iPlus1
    grammar.swapPositionsInHierarchy(T_i, Z_i);

    // nonterminal Z_i won't be used any longer; it can be replaced
    index_t Z_iPlus1 = reuseNonterminal(A_i, T_i, Z_i);
    return Z_iPlus1;
}

index_t AddPairOperation::nextActiveRuleCase3(
        index_t Z_i, index_t Y_iPlus1, index_t A_i, index_t B_i)
{
    production_t B_iProd = grammar.getProd(B_i);
    B_iProd = adjustProductionToOperationType(B_iProd);
    index_t U_i = B_iProd.first;
    index_t V_i = B_iProd.second;
    // nonterminal Z_i won't be used any longer; it can be replaced
    index_t P_i = reuseNonterminal(A_i, U_i, Z_i);
    //
    index_t Q_i = B_i > maxSymbolAtBeginningOfOperation
            ? reuseNonterminal(V_i, Y_iPlus1, B_i)
            : adjustedNewNonterminal(V_i, Y_iPlus1);

    index_t Z_iPlus1 = adjustedNewNonterminal(P_i, Q_i);
    return Z_iPlus1;
}

index_t AddPairOperation::nextActiveRuleCase2Or3(index_t Z_i, index_t Y_iPlus1)
{
    production_t Z_iProd = grammar.getProd(Z_i);
    Z_iProd = adjustProductionToOperationType(Z_iProd);
    index_t A_i = Z_iProd.first;
    index_t B_i = Z_iProd.second;
    index_t Z_iPlus1;

    production_t T_iProd = production_t(B_i, Y_iPlus1);
    T_iProd = adjustProductionToOperationType(T_iProd);

    Z_iPlus1 = checkLowerAlphaBound(A_i, T_iProd)
            ? nextActiveRuleCase2(Z_i, A_i, T_iProd)
            : nextActiveRuleCase3(Z_i, Y_iPlus1, A_i, B_i);

    return Z_iPlus1;
}
