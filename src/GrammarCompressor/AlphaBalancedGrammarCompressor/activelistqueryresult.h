#ifndef ACTIVELISTQUERYRESULT_H
#define ACTIVELISTQUERYRESULT_H

#include <vector>
#include "types.h"

struct ActiveListQueryResult
{
    bool XContainsWholeFactor;

    index_t X;
    std::vector<index_t> ASequence;
    index_t Y;

    index_t posOfFactorInX;
    std::map<index_t, index_t>::iterator ASequenceBeginIterInActiveList;
    std::map<index_t, index_t>::iterator ASequenceEndIterInActiveList;
    index_t lengthOfFactorSuffixInY;
};

#endif // ACTIVELISTQUERYRESULT_H
