#include "avlbasedgrammarcompressor.h"
#include "lzcompressor.h"
#include "factor.h"
#include "grammardecomposeroffactor.h"
#include "grammardecomposition.h"

AvlBasedGrammarCompressor::AvlBasedGrammarCompressor(
        std::vector<terminal_t> &word)
    : word(word)
    , leftMergeOperation(MergeOperation(TreeOperation::LEFT, grammar))
    , rightMergeOperation(MergeOperation(TreeOperation::RIGHT, grammar))
{

}

void AvlBasedGrammarCompressor::constructGrammar()
{
    std::vector<Factor> lz = LZCompressor::noSelfRef(word);
    word.clear();
    index_t root = lz.front().getFreeLetter();
    GrammarDecomposerOfFactor decomposer(grammar);

    for (index_t i = 1; i < lz.size(); i++) {
        Factor &factor = lz[i];

        index_t factorRoot;
        if (factor.isFreeLetter()) {
            factorRoot = factor.getFreeLetter();
        } else {
            GrammarDecomposition decomposition =
                    decomposer.decomposeFactor(factor);
            factorRoot = mergeDecomposition(decomposition);
        }
        root = merge(root, factorRoot);

        index_t c = 4;
        if ((i > c) && (i % ((lz.size()+1) / c) == 0)) {
            // there are many unused nonterminals after merging trees
            // they accumulate and should be regularly removed
            grammar.removeUnusedNonterminals();
            root = grammar.getMaxSymbol();
        }
    }

    grammar.removeUnusedNonterminals();
}

Grammar &AvlBasedGrammarCompressor::getGrammar()
{
    return grammar;
}

index_t AvlBasedGrammarCompressor::merge(
        index_t leftNonterminal, index_t rightNonterminal)
{
    if (grammar.areBalanced(leftNonterminal, rightNonterminal))
        return grammar.newNonterminal(leftNonterminal, rightNonterminal);

    if (grammar.getHeight(leftNonterminal)
            >= grammar.getHeight(rightNonterminal))
        return rightMergeOperation.perform(leftNonterminal, rightNonterminal);
    
    return leftMergeOperation.perform(leftNonterminal, rightNonterminal);
}
