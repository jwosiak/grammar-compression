#ifndef MERGEOPERATION_H
#define MERGEOPERATION_H

#include "treeoperation.h"
#include "rotationoperation.h"
#include "types.h"

class GrammarWithLengthsAndHeights;

class MergeOperation : TreeOperation
{
    GrammarWithLengthsAndHeights &grammar;

    RotationOperation leftRotationOperation;
    RotationOperation rightRotationOperation;

public:
    MergeOperation(OperationType operationType,
                   GrammarWithLengthsAndHeights &grammar);
    virtual ~MergeOperation();

    index_t perform(index_t A, index_t B);

protected:
    // method used by the base class
    Grammar &getGrammar();

    index_t rotate(index_t B, index_t D);

    index_t getNextNodeFromOuterBranch(index_t currentNode);
    std::vector<index_t> traverseOuterBranchUntilSufficientHeight(
            index_t root, uint8_t sufficientHeight);
    index_t rebalanceOuterBranch(std::vector<index_t> &nodesToRebalance);
};

#endif // MERGEOPERATION_H
