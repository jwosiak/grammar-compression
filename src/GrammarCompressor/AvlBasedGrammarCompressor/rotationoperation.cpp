#include "rotationoperation.h"
#include "grammarwithlengthsandheights.h"

RotationOperation::RotationOperation(
        OperationType operationType, GrammarWithLengthsAndHeights &grammar)
    : TreeOperation (OperationType::RIGHT, operationType)
    , grammar(grammar)
{

}

RotationOperation::~RotationOperation()
{

}

index_t RotationOperation::perform(
        index_t B, index_t D)
{
    production_t DProd = grammar.getProd(D);
    DProd = adjustProductionToOperationType(DProd);
    index_t C = DProd.first;
    index_t E = DProd.second;

    index_t APrim = grammar.getHeight(E) > grammar.getHeight(C)
            ? rotationForOverbalancedE(B, C, E)
            : rotationForOverbalancedC(B, C, E);
    return APrim;
}

Grammar &RotationOperation::getGrammar()
{
    return grammar;
}

index_t RotationOperation::rotationForOverbalancedE(
        index_t B, index_t C, index_t E)
{
    index_t DPrim = adjustedNewNonterminal(B, C);
    index_t APrim = adjustedNewNonterminal(DPrim, E);
    return APrim;
}

index_t RotationOperation::rotationForOverbalancedC(
        index_t B, index_t C, index_t E)
{
    production_t CProd = grammar.getProd(C);
    CProd = adjustProductionToOperationType(CProd);
    index_t F = CProd.first;
    index_t G = CProd.second;

    index_t CPrim = adjustedNewNonterminal(B, F);
    index_t DPrim = adjustedNewNonterminal(G, E);
    index_t APrim = adjustedNewNonterminal(CPrim, DPrim);
    return APrim;
}
