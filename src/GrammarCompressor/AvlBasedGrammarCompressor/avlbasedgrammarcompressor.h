#ifndef AVLBASEDGRAMMARCOMPRESSOR_H
#define AVLBASEDGRAMMARCOMPRESSOR_H

#include "treemerginggrammarcompressor.h"
#include <vector>
#include "types.h"
#include "grammarwithlengthsandheights.h"
#include "mergeoperation.h"

class AvlBasedGrammarCompressor : public TreeMergingGrammarCompressor
{
private:
    std::vector<terminal_t> &word;

    GrammarWithLengthsAndHeights grammar;

    MergeOperation leftMergeOperation;
    MergeOperation rightMergeOperation;

public:
    AvlBasedGrammarCompressor(std::vector<terminal_t> &word);

    // GrammarCompressor interface
    virtual void constructGrammar();
    virtual Grammar &getGrammar();

protected:
    // TreeMergingGrammarCompressor interface
    virtual index_t merge(index_t leftNonterminal, index_t rightNonterminal);
};

#endif // AVLBASEDGRAMMARCOMPRESSOR_H
