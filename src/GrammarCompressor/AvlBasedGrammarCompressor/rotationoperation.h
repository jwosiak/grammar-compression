#ifndef ROTATIONOPERATION_H
#define ROTATIONOPERATION_H

#include "treeoperation.h"
#include "types.h"

class GrammarWithLengthsAndHeights;

class RotationOperation : public TreeOperation
{
    GrammarWithLengthsAndHeights &grammar;

public:
    RotationOperation(OperationType operationType,
                      GrammarWithLengthsAndHeights &grammar);
    virtual ~RotationOperation();

    virtual index_t perform(index_t B, index_t D);
protected:
    // method used by the base class
    Grammar &getGrammar();

    index_t rotationForOverbalancedE(
            index_t B, index_t C, index_t E);
    index_t rotationForOverbalancedC(
            index_t B, index_t C, index_t E);
};

#endif // ROTATIONOPERATION_H
