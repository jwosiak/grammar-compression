#include "mergeoperation.h"
#include "grammarwithlengthsandheights.h"
#include <vector>

MergeOperation::MergeOperation(
        OperationType operationType, GrammarWithLengthsAndHeights &grammar)
    : TreeOperation (OperationType::RIGHT, operationType)
    , grammar(grammar)
    , leftRotationOperation(RotationOperation(TreeOperation::LEFT, grammar))
    , rightRotationOperation(RotationOperation(TreeOperation::RIGHT, grammar))
{

}

MergeOperation::~MergeOperation()
{

}

index_t MergeOperation::perform(index_t A, index_t B)
{
    swapNonterminalsIfNotBaseCase(A, B);

    std::vector<index_t> outerBranchPrefix =
            traverseOuterBranchUntilSufficientHeight(A, grammar.getHeight(B));
    index_t BParentAtOuterBranch =
            adjustedNewNonterminal(outerBranchPrefix.back(), B);
    outerBranchPrefix.back() = BParentAtOuterBranch;

    index_t newRoot = rebalanceOuterBranch(outerBranchPrefix);
    return newRoot;
}

Grammar &MergeOperation::getGrammar()
{
    return grammar;
}

index_t MergeOperation::rotate(index_t B, index_t D)
{
    index_t APrim = operationType == TreeOperation::LEFT
            ? leftRotationOperation.perform(B, D)
            : rightRotationOperation.perform(B, D);
    return APrim;
}

index_t MergeOperation::getNextNodeFromOuterBranch(index_t currentNode)
{
    production_t currentNodeProd = grammar.getProd(currentNode);
    currentNodeProd = adjustProductionToOperationType(currentNodeProd);
    return currentNodeProd.second;
}

std::vector<index_t> MergeOperation::traverseOuterBranchUntilSufficientHeight(
        index_t root, uint8_t sufficientHeight)
{
    std::vector<index_t> outerBranchPrefix;
    outerBranchPrefix.reserve(grammar.getHeight(root));
    outerBranchPrefix.push_back(root);

    index_t secondLastNode = root;
    index_t lastNode = getNextNodeFromOuterBranch(secondLastNode);

    while (grammar.getHeight(lastNode) - sufficientHeight > 1) {
        secondLastNode = lastNode;
        lastNode = getNextNodeFromOuterBranch(secondLastNode);
        outerBranchPrefix.push_back(secondLastNode);
    }

    outerBranchPrefix.push_back(lastNode);
    return outerBranchPrefix;
}

index_t MergeOperation::rebalanceOuterBranch(
        std::vector<index_t> &nodesToRebalance)
{
    index_t lastNode = nodesToRebalance.back();
    nodesToRebalance.pop_back();

    while (!nodesToRebalance.empty()) {
        index_t secondLastNode = nodesToRebalance.back();
        nodesToRebalance.pop_back();

        production_t secondLastNodeProd = grammar.getProd(secondLastNode);
        secondLastNodeProd =
                adjustProductionToOperationType(secondLastNodeProd);
        index_t brotherOfLastNode = secondLastNodeProd.first;

        secondLastNode = grammar.areBalanced(brotherOfLastNode, lastNode)
                ? adjustedNewNonterminal(brotherOfLastNode, lastNode)
                : rotate(brotherOfLastNode, lastNode);

        lastNode = secondLastNode;
    }

    return lastNode;
}
