#include "treeoperation.h"
#include "grammar.h"

TreeOperation::TreeOperation(OperationType baseCaseOperationType,
                             OperationType operationType)
    : baseCaseOperationType(baseCaseOperationType)
    , operationType(operationType)
{

}

TreeOperation::~TreeOperation()
{

}

void TreeOperation::swapNonterminalsIfNotBaseCase(
        index_t &leftNonterminal, index_t &rightNonterminal)
{
    if (operationType != baseCaseOperationType)
        std::swap(leftNonterminal, rightNonterminal);
}

production_t &TreeOperation::adjustProductionToOperationType(production_t &prod)
{
    swapNonterminalsIfNotBaseCase(prod.first, prod.second);
    return prod;
}

index_t TreeOperation::adjustedNewNonterminal(
        index_t leftProd, index_t rightProd)
{
    production_t theNonterminalProd(leftProd, rightProd);
    theNonterminalProd =
            adjustProductionToOperationType(theNonterminalProd);
    index_t theNonterminal = getGrammar().newNonterminal(theNonterminalProd);
    return theNonterminal;
}

index_t TreeOperation::reuseNonterminal(
        index_t leftProd, index_t rightProd, index_t unusedNonterminal)
{
    production_t nonterminalNewProduction(leftProd, rightProd);
    nonterminalNewProduction =
            adjustProductionToOperationType(nonterminalNewProduction);
    getGrammar().setProd(unusedNonterminal, nonterminalNewProduction);
    return unusedNonterminal;
}
