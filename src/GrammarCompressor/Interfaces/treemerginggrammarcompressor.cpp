#include "treemerginggrammarcompressor.h"
#include "grammardecomposition.h"

TreeMergingGrammarCompressor::~TreeMergingGrammarCompressor()
{

}

index_t TreeMergingGrammarCompressor::mergeAll(
        std::forward_list<index_t> &trees, bool leftOrder)
{
    // merges nonterminals (subtrees) from left to right if the left order
    //   otherwise, merges from right to left

    index_t T = trees.front();
    trees.pop_front();

    while (!trees.empty()) {
        index_t G = T;
        T = trees.front();
        trees.pop_front();

        T = leftOrder? merge(G, T) : merge(T, G);
    }

    return T;
}

index_t TreeMergingGrammarCompressor::mergeDecomposition(
        GrammarDecomposition &decomposition)
{
    if (decomposition.isBitonic())
        return mergeBitonicDecomposition(decomposition);
    return mergeOneSidedDecomposition(decomposition, true);
}

index_t TreeMergingGrammarCompressor::mergeOneSidedDecomposition(
        GrammarDecomposition &decomposition, bool leftOrder)
{
    if (leftOrder)
        return mergeAll(decomposition.getLeftDecomposition(), true);
    return mergeAll(decomposition.getRightDecomposition(), false);
}

index_t TreeMergingGrammarCompressor::mergeBitonicDecomposition(
        GrammarDecomposition &bitonicDecomposition)
{
    index_t leftRoot =
            mergeAll(bitonicDecomposition.getLeftDecomposition(), true);
    index_t rightRoot =
            mergeAll(bitonicDecomposition.getRightDecomposition(), false);
    return merge(leftRoot, rightRoot);
}
