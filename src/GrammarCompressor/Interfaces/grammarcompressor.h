#ifndef GRAMMARCOMPRESSOR_H
#define GRAMMARCOMPRESSOR_H

class Grammar;

class GrammarCompressor
{
public:
    virtual ~GrammarCompressor();
    virtual void constructGrammar() = 0;
    virtual Grammar& getGrammar() = 0;
};


#endif // GRAMMARCOMPRESSOR_H
