#ifndef TREEOPERATION_H
#define TREEOPERATION_H

#include "types.h"

class Grammar;

// this class generalizes some parts of operations on
//   AVL based grammars and alpha-balanced grammars

// these operations are directed (left / right) and
//   the cases are symmetrical

// the base case is the directed operation described
//   in a paper

// to obtain the symmetrical case we need only to
//   'adjust' productions of nonterminals used/created during
//   an operation

// 'adjust' == to swap left and right productions of a nonterminal

class TreeOperation
{
public:
    enum OperationType
    {
        // left operation means that it is performed from left to right
        //   e.g. merging a tree on the left to a tree on the right
        LEFT,
        // operations from right to left
        RIGHT
    };

protected:
    // const for an implementation
    const OperationType baseCaseOperationType;
    // const for an instance
    const OperationType operationType;

    TreeOperation(OperationType baseCaseOperationType,
                  OperationType operationType);
public:
    virtual ~TreeOperation();

    virtual index_t perform(
            index_t leftNonterminal, index_t rightNonterminal) = 0;

protected:
    virtual Grammar &getGrammar() = 0;

    void swapNonterminalsIfNotBaseCase(
            index_t &leftNonterminal, index_t &rightNonterminal);
    production_t &adjustProductionToOperationType(production_t &prod);
    index_t adjustedNewNonterminal(index_t leftProd, index_t rightProd);
    index_t reuseNonterminal(
            index_t leftProd, index_t rightProd, index_t unusedNonterminal);
};

#endif // TREEOPERATION_H
