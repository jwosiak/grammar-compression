#ifndef TREEMERGINGGRAMMARCOMPRESSOR_H
#define TREEMERGINGGRAMMARCOMPRESSOR_H

#include "grammarcompressor.h"
#include "types.h"
#include <forward_list>

class GrammarDecomposition;

// interface of algorithms based on finding grammar decomposition
//  of a factor and merging that decomposition into a single nonterminal

class TreeMergingGrammarCompressor : public GrammarCompressor
{
public:
    virtual ~TreeMergingGrammarCompressor();

protected:
    // merges two nonterminals
    virtual index_t merge(
            index_t leftNonterminal, index_t rightNonterminal) = 0;
    // merges a decomposition of a substring in an efficient way
    index_t mergeAll(std::forward_list<index_t> &trees, bool leftOrder);
    index_t mergeDecomposition(GrammarDecomposition &decomposition);
    index_t mergeOneSidedDecomposition(
            GrammarDecomposition &decomposition, bool leftOrder);
private:
    index_t mergeBitonicDecomposition(
            GrammarDecomposition &bitonicDecomposition);
};

#endif // TREEMERGINGGRAMMARCOMPRESSOR_H
