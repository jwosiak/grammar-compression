#-------------------------------------------------
#
# Project created by QtCreator 2019-04-25T14:52:43
#
#-------------------------------------------------

TEMPLATE = subdirs

SUBDIRS += \
    LZCompressor \
    Interfaces \
    RecompressGrammarCompressor \
    ReallySimpleGrammarCompressor \
    AlphaBalancedGrammarCompressor \
    AvlBasedGrammarCompressor \

CONFIG += ordered

unix {
    target.path = /usr/lib
    INSTALLS += target
}
