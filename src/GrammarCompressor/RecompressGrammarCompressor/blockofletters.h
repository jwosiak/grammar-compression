#ifndef BLOCKOFLETTERS_H
#define BLOCKOFLETTERS_H

#include "types.h"

struct BlockOfLetters
{
public:
    index_t position;
    index_t length;
    index_t baseLetter;
    index_t nonterminalProducingThisBlock;

    BlockOfLetters();

    BlockOfLetters(index_t position, index_t length, index_t baseLetter);

    inline static bool compByPosition(const BlockOfLetters &b1,
                                      const BlockOfLetters &b2)
    {
        return b1.position < b2.position;
    }

    inline static bool compByLength(const BlockOfLetters &b1,
                                    const BlockOfLetters &b2)
    {
        return b1.length < b2.length;
    }

    inline static bool compByBaseLetter(const BlockOfLetters &b1,
                                        const BlockOfLetters &b2)
    {
        return b1.baseLetter < b2.baseLetter;
    }
};

#endif // BLOCKOFLETTERS_H
