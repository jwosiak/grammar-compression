#include "alphabetdisjointpartitioner.h"
#include "grammar.h"

AlphabetPartition AlphabetDisjointPartitioner::getOutputPartition() const
{
    return outputPartition;
}

AlphabetDisjointPartitioner::AlphabetDisjointPartitioner(const Grammar &grammar)
    : emptyLetter(grammar.getEmptyLetter())
    , outputPartition(AlphabetPartition(grammar.getMaxSymbol()))
{
    for (int i = 0; i < 2; i++)
        count[i].resize(grammar.getMaxSymbol()+1);
}

void AlphabetDisjointPartitioner::assignLetterToPartition(index_t letter)
{
    int choice = count[RIGHT][letter] >= count[LEFT][letter]? LEFT : RIGHT;
    outputPartition.subpartitionsMasks[choice][letter] = true;
}

void AlphabetDisjointPartitioner::setOptimalOrderOfSubpartitionsForWord(
        const std::vector<index_t> &word)
{
    int orderImbalance = computeSubpartitionsOrderImbalanceForWord(word);
    if (orderImbalance > 0) {
        outputPartition.swapSubpartitions();
        std::swap(count[LEFT], count[RIGHT]);
    }
}

AlphabetDisjointPartitioner::side
    AlphabetDisjointPartitioner::getLetterSideAssignment(index_t letter)
{
    if (outputPartition.subpartitionsMasks[LEFT][letter])
        return LEFT;

    return RIGHT;
}

int AlphabetDisjointPartitioner::computeSubpartitionsOrderImbalanceForWord(
        const std::vector<index_t> &word)
{
    int imbalance = 0;
    for (index_t i = 0; i < word.size()-1; i++) {
        index_t leftLetter = word[i];
        index_t rightLetter = word[i+1];

        // we consider an occurrence of pair ('leftLetter', 'rightLetter')

        if (outputPartition.pairFitsPartition(leftLetter, rightLetter))
            imbalance--;
        if (outputPartition.pairFitsReversedPartition(leftLetter, rightLetter))
            imbalance++;
    }
    return imbalance;
}
