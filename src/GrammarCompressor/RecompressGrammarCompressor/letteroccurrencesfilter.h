#ifndef LETTEROCCURRENCESFILTER_H
#define LETTEROCCURRENCESFILTER_H

#include "types.h"
#include "grammar.h"
#include "letteroccurrencecontext.h"
#include "alphabetpartition.h"

#include <vector>
#include <algorithm>

template <typename T>
class LetterOccurrencesFilter
{
    const std::vector<index_t> &word;

    // needed by counting sort
    const index_t maxLetter;

    // special letter, 'occurrs' before the first
    //   and after the last letter in 'w'
    const index_t emptyLetter;

    // countOfLessOrEqualValues[letter] = number of occurrences
    //   of the letter and occurrences of smaller letters;
    // it is computed during counting sort of occurrences
    std::vector<index_t> countOfLessOrEqualValues;

public:
    std::vector<LetterOccurrenceContext<T>> *sortedOccurrences;

    LetterOccurrencesFilter(
            const std::vector<index_t> &word, const Grammar &grammar);
    ~LetterOccurrencesFilter();

    std::vector<LetterOccurrenceContext<T> > getOccurrenceContextsOfLetter(
            index_t letter);

    void sortOccurrenceContextsByCentralLetters();
    void sortOccurrenceContextsByPredessors();

    void filterOccurrencesFittingPartition(const AlphabetPartition &partition);
private:
    std::vector<index_t> &computeCountOfLessOrEqualValues();

    void assignOccurrenceFromWord(
            LetterOccurrenceContext<T> &occurrenceToBeAssign,
            index_t posFromWord);
};

template<typename T>
LetterOccurrencesFilter<T>::LetterOccurrencesFilter(
        const std::vector<index_t> &word, const Grammar &grammar)
    : word(word)
    , maxLetter(grammar.getMaxSymbol())
    , emptyLetter(grammar.getEmptyLetter())
{

}

template<typename T>
LetterOccurrencesFilter<T>::~LetterOccurrencesFilter()
{
    delete sortedOccurrences;
}

template<typename T>
std::vector<LetterOccurrenceContext<T> >
    LetterOccurrencesFilter<T>::getOccurrenceContextsOfLetter(index_t letter)
{
    index_t endPos = countOfLessOrEqualValues[letter];
    index_t beginPos = letter > 0? countOfLessOrEqualValues[letter-1] : 0;

    auto beginIter = sortedOccurrences->begin() + beginPos;
    auto endIter = sortedOccurrences->begin() + endPos;
    return std::vector<LetterOccurrenceContext<T> >(beginIter, endIter);
}

template<typename T>
void LetterOccurrencesFilter<T>::sortOccurrenceContextsByCentralLetters()
{
    sortedOccurrences =
            new std::vector<LetterOccurrenceContext<T>>(word.size());

    this->countOfLessOrEqualValues = computeCountOfLessOrEqualValues();

    // use a copy, because it will be overwritten
    std::vector<index_t> copyOfCountOfLessOrEqualValues(
                this->countOfLessOrEqualValues);

    // construct LetterOccurrenceContexts and assign them to sorted vector
    for (index_t j = word.size(); j > 0; j--) {
        index_t preSortedPosition = j - 1;
        index_t postSortedPosition =
                copyOfCountOfLessOrEqualValues[word[preSortedPosition]]-1;

        LetterOccurrenceContext<T> &toAssign =
                sortedOccurrences->at(postSortedPosition);
        assignOccurrenceFromWord(toAssign, preSortedPosition);

        copyOfCountOfLessOrEqualValues[word[preSortedPosition]]--;
    }
}

template<typename T>
void LetterOccurrencesFilter<T>::sortOccurrenceContextsByPredessors()
{
    std::vector<index_t> countOfLessOrEqualPredecessorValues(maxLetter+1);
    auto *sortedOccsByPredessors =
            new std::vector<LetterOccurrenceContext<T>>(
                sortedOccurrences->size());

    // count predecessor in occurrence contexts
    for (index_t i = 0; i < sortedOccsByPredessors->size(); i++) {
        index_t pred = sortedOccurrences->at(i).predecessor;
        countOfLessOrEqualPredecessorValues[pred]++;
    }

    for (index_t a = 1; a <= maxLetter; a++)
        countOfLessOrEqualPredecessorValues[a] +=
                countOfLessOrEqualPredecessorValues[a-1];

    // sort occurrence contexts by their predecessors
    for (index_t j = sortedOccurrences->size(); j > 0; j--) {
        index_t preSortedPosition = j - 1;
        LetterOccurrenceContext<T> &val =
                sortedOccurrences->at(preSortedPosition);
        index_t postSortedPosition =
                countOfLessOrEqualPredecessorValues[val.predecessor]-1;

        sortedOccsByPredessors->at(postSortedPosition) = val;
        countOfLessOrEqualPredecessorValues[val.predecessor]--;
    }

    delete this->sortedOccurrences;
    this->sortedOccurrences = sortedOccsByPredessors;
}

template<typename T>
void LetterOccurrencesFilter<T>::filterOccurrencesFittingPartition(
        const AlphabetPartition &partition)
{
    index_t postFilterIter = 0;
    for (index_t preFilterIter = 0; preFilterIter < sortedOccurrences->size()
         ; preFilterIter++) {
        LetterOccurrenceContext<T> &currOcc =
                sortedOccurrences->at(preFilterIter);

        if ((currOcc.predecessor != emptyLetter)
                && partition.pairFitsPartition(currOcc.predecessor,
                                               currOcc.central))
            sortedOccurrences->at(postFilterIter++) =
                    sortedOccurrences->at(preFilterIter);
    }
    sortedOccurrences->resize(postFilterIter);
}

template<typename T>
std::vector<index_t>
    &LetterOccurrencesFilter<T>::computeCountOfLessOrEqualValues()
{
    countOfLessOrEqualValues.resize(maxLetter+1);

    for (index_t j = 0; j < word.size(); j++)
        countOfLessOrEqualValues[word[j]]++;

    for (index_t a = 1; a <= maxLetter; a++)
        countOfLessOrEqualValues[a] += countOfLessOrEqualValues[a-1];

    return countOfLessOrEqualValues;
}

template<typename T>
void LetterOccurrencesFilter<T>::assignOccurrenceFromWord(
        LetterOccurrenceContext<T> &occurrenceToAssign, index_t posFromWord)
{
    occurrenceToAssign.central = word[posFromWord];
    occurrenceToAssign.pointer = posFromWord;

    occurrenceToAssign.predecessor =
            posFromWord == 0 ? emptyLetter : word[posFromWord-1];
    occurrenceToAssign.successor =
            posFromWord == word.size()-1 ? emptyLetter : word[posFromWord+1];
}


#endif // LETTEROCCURRENCESFILTER_H
