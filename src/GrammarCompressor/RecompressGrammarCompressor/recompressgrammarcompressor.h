#ifndef RECOMPRESSGRAMMARCOMPRESSOR_H
#define RECOMPRESSGRAMMARCOMPRESSOR_H

#include "grammarcompressor.h"
#include "grammar.h"
#include <vector>


class RecompressGrammarCompressor : public GrammarCompressor
{
    std::vector<index_t> word;
    Grammar grammar;

public:
    RecompressGrammarCompressor(std::vector<terminal_t> &word);

    // GrammarCompressor interface
    virtual void constructGrammar();
    virtual Grammar &getGrammar();

private:

    enum RequiredBits
    {
        EIGHT = 1,
        SIXTEEN = 2,
        THIRTY_TWO = 4
    };

    RequiredBits calculateNumberOfBitsToRepresentAlphabet();
};


#endif // RECOMPRESSGRAMMARCOMPRESSOR_H
