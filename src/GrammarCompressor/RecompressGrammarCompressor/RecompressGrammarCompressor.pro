QT       -= gui

TARGET = RecompressGrammarCompressor
TEMPLATE = lib
CONFIG += staticlib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    alphabetdisjointpartitioner.cpp \
    alphabetpartition.cpp \
    blockcompressor.cpp \
    blockofletters.cpp \
    blocktogrammardecomposer.cpp \
    letteroccurrencecontext.cpp \
    letteroccurrencesfilter.cpp \
    paircompressor.cpp \
    recompressgrammarcompressor.cpp \


HEADERS += \
    alphabetdisjointpartitioner.h \
    alphabetpartition.h \
    blockcompressor.h \
    blockofletters.h \
    blocktogrammardecomposer.h \
    letteroccurrencecontext.h \
    letteroccurrencesfilter.h \
    paircompressor.h \
    recompressgrammarcompressor.h \


unix {
    target.path = /usr/lib
    INSTALLS += target
}


INCLUDEPATH += $$PWD/../../Grammar
INCLUDEPATH += $$PWD/../../Tools/Timer
INCLUDEPATH += $$PWD/../../Tools/GenTools
INCLUDEPATH += $$PWD/../../Tools/Types
INCLUDEPATH += $$PWD/../../SuffixArrayLib
INCLUDEPATH += $$PWD/../LZCompressor
INCLUDEPATH += $$PWD/../Interfaces
