#ifndef LETTEROCCURRENCECONTEXT_H
#define LETTEROCCURRENCECONTEXT_H

#include "types.h"

template <typename T>
struct LetterOccurrenceContext
{
    T central;
    index_t pointer;

    T predecessor;
    T successor;

    LetterOccurrenceContext()
    {

    }

    LetterOccurrenceContext(
            T predessor, T central, T successor, index_t pointer)
        : predecessor(predessor)
        , central(central)
        , successor(successor)
        , pointer(pointer)
    {

    }
};


#endif // LETTEROCCURRENCECONTEXT_H
