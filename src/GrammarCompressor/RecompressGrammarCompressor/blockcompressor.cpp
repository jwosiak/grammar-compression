#include "blockcompressor.h"
#include "grammar.h"
#include "blockofletters.h"

BlockCompressor::BlockCompressor(std::vector<index_t> &word, Grammar &grammar)
    : word(word)
    , grammar(grammar)
    , blockDecomposer(BlockToGrammarDecomposer(grammar))
{

}

std::vector<index_t> &BlockCompressor::compressBlocks()
{
    std::vector<BlockOfLetters> blocks = getBlocksFromWord();

    if (!blocks.empty())
        // changes every c^k into a new letter
        word = compressBlocks(blocks);

    return word;
}

bool BlockCompressor::isNextBlockStarting(
        BlockOfLetters &currentBlock, index_t newlyEncounteredLetter)
{
    return currentBlock.baseLetter != newlyEncounteredLetter;
}

std::vector<BlockOfLetters> &BlockCompressor::finishProcessingBlock(
        BlockOfLetters &currentBlock, std::vector<BlockOfLetters> &blocks)
{
    if (currentBlock.length > 1)
        blocks.push_back(currentBlock);
    return blocks;
}

std::vector<BlockOfLetters> BlockCompressor::getBlocksFromWord()
{
    std::vector<BlockOfLetters> blocks;
    BlockOfLetters currentBlock = BlockOfLetters(0, 1, word[0]);

    for (index_t wordIter = 1; wordIter < word.size(); wordIter++) {
        index_t newlyEncounteredLetter = word[wordIter];

        if (isNextBlockStarting(currentBlock, newlyEncounteredLetter)) {
            blocks = finishProcessingBlock(currentBlock, blocks);
            currentBlock = BlockOfLetters(wordIter, 1, newlyEncounteredLetter);
        } else {
            currentBlock.length++;
        }
    }

    blocks = finishProcessingBlock(currentBlock, blocks);
    return blocks;
}

std::vector<index_t> &BlockCompressor::replaceBlocksWithLetters(
        std::vector<BlockOfLetters> &blocks)
{
    // rewriting in-place the word (replace blocks,
    //   they have representations in the grammar)

    // we iterate the word from left to right and encounter blocks
    //   we start iterating from the position of the first block
    index_t blocksIter = 0;
    BlockOfLetters& encounteredBlock = blocks[blocksIter];
    index_t postReplacementIter = encounteredBlock.position;

    for (index_t preReplacementIter = postReplacementIter
         ; preReplacementIter < word.size()
         ; postReplacementIter++, preReplacementIter++) {
        if (blocksIter < blocks.size())
            encounteredBlock = blocks[blocksIter];

        // if a block starts there
        if (encounteredBlock.position == preReplacementIter) {
            // then put a single letter instead of the block
            word[postReplacementIter] =
                    encounteredBlock.nonterminalProducingThisBlock;
            //skip positions to the end of the block
            preReplacementIter += encounteredBlock.length - 1;
            blocksIter++;
        } else {
            // the letter at the position doesn't change
            word[postReplacementIter] = word[preReplacementIter];
        }
    }

    // set the new size for the word
    word.resize(postReplacementIter);
    return word;
}

std::vector<BlockOfLetters> BlockCompressor::lexSortBlocks(
        std::vector<BlockOfLetters> &blocks)
{
    std::stable_sort(blocks.begin(), blocks.end(),
                     BlockOfLetters::compByLength);
    std::stable_sort(blocks.begin(), blocks.end(),
                     BlockOfLetters::compByBaseLetter);
    return blocks;
}

Grammar &BlockCompressor::addToGrammarRulesProducingBlocks(
        std::vector<BlockOfLetters> &blocks)
{
    BlockOfLetters *lastProcessedBlock = &blocks[0];
    blockDecomposer.reset(lastProcessedBlock->baseLetter);

    for (BlockOfLetters &currentBlock : blocks) {

        // decomposes blocks of concrete letter until encounters
        //   a block of another letter;
        //   that concrete letter won't be encountered again;
        if (lastProcessedBlock->baseLetter != currentBlock.baseLetter)
            blockDecomposer.reset(currentBlock.baseLetter);

        // sets for the currentBlock the letter which is the
        //   nonterminal of the grammar and produces the currentBlock;
        //   the side effect of this operation is that there are created
        //   new nonterminals
        currentBlock.nonterminalProducingThisBlock =
                blockDecomposer.decomposeNext(currentBlock.length);

        lastProcessedBlock = &currentBlock;
    }
    return grammar;
}

std::vector<index_t> &BlockCompressor::compressBlocks(
        std::vector<BlockOfLetters> &blocks)
{
    // sorts blocks lexicographically (block.baseLetter, block.length)
    blocks = lexSortBlocks(blocks);

    grammar = addToGrammarRulesProducingBlocks(blocks);

    // it is convenient to replace blocks in the word when blocks
    //   are sorted by their position in the word
    std::stable_sort(blocks.begin(), blocks.end(),
                     BlockOfLetters::compByPosition);
    word = replaceBlocksWithLetters(blocks);

    return word;
}
