#ifndef BLOCKTOGRAMMARDECOMPOSER_H
#define BLOCKTOGRAMMARDECOMPOSER_H

#include "types.h"
#include "unordered_map"
#include <functional>

class Grammar;

// it is used to find grammar decomposition of a sequence
//   (elements of the sequence appears online, their lengths
//    are non-decreasing) of blocks of a letter
class BlockToGrammarDecomposer
{
    index_t blockBaseLetter;
    // key: block's length -> value: nonterminal which produces that block
    std::unordered_map<index_t,index_t> alreadyDecomposedBlocks;
    index_t maxOfAlreadyDecomposedBlocks;

    Grammar &grammar;

public:
    BlockToGrammarDecomposer(Grammar &grammar);

    void reset(index_t blockBaseLetter);
    index_t decomposeNext(index_t blockLength);

private:
    bool isAlreadyDecomposed(index_t blockLength);
    index_t calculateHighestPowerOfTwoNotGreaterThan(index_t parentRuleLength);
};

#endif // BLOCKTOGRAMMARDECOMPOSER_H
