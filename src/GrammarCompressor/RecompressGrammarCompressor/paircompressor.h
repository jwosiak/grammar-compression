#ifndef PAIRCOMPRESSOR_H
#define PAIRCOMPRESSOR_H

#include "alphabetpartition.h"
#include "alphabetdisjointpartitioner.h"
#include "letteroccurrencesfilter.h"
#include "grammar.h"


template<typename T>
class PairCompressor
{
    std::vector<index_t> &word;
    Grammar &grammar;

    // is used to localize an occurrence (and it's closest neighbourhood)
    //   of a letter
    LetterOccurrencesFilter<T> pairFilter;
public:
    PairCompressor(std::vector<index_t> &word, Grammar &grammar);

    std::vector<index_t> &compressPairs();

private:
    AlphabetPartition getGreedyAlphabetPartition();

    std::vector<index_t> &removePositionsFromWord(
            std::vector<bool> &toRemoveMask);
    std::vector<index_t> &compressFilteredPairs();
};

template<typename T>
PairCompressor<T>::PairCompressor(std::vector<index_t> &word, Grammar &grammar)
    : word(word)
    , grammar(grammar)
    , pairFilter(LetterOccurrencesFilter<T>(word, grammar))
{

}

template<typename T>
std::vector<index_t> &PairCompressor<T>::compressPairs()
{
    // do neccesary sorting
    pairFilter.sortOccurrenceContextsByCentralLetters();
    AlphabetPartition partition = getGreedyAlphabetPartition();

    // preprocess before compression
    pairFilter.filterOccurrencesFittingPartition(partition);
    pairFilter.sortOccurrenceContextsByPredessors();

    // pairs were filtered during preprocessing
    word = compressFilteredPairs();
    return word;
}

template<typename T>
AlphabetPartition PairCompressor<T>::getGreedyAlphabetPartition()
{
    AlphabetDisjointPartitioner partitioner(grammar);

    for (index_t letter = 0; letter <= grammar.getMaxSymbol(); letter++) {
        partitioner.assignLetterToPartition(letter);
        for (const LetterOccurrenceContext<T> &occurrenceContext
             : pairFilter.getOccurrenceContextsOfLetter(letter))
            partitioner.contributeOccurrences<T>(occurrenceContext);
    }

    partitioner.setOptimalOrderOfSubpartitionsForWord(word);
    return partitioner.getOutputPartition();
}

template<typename T>
std::vector<index_t> &PairCompressor<T>::removePositionsFromWord(
        std::vector<bool> &toRemoveMask)
{
    // rewrite word without letters at specified positions
    index_t postRemoveIter = 0;
    for (index_t preRemoveIter = 0; preRemoveIter < word.size()
         ; preRemoveIter++)
        if (!toRemoveMask[preRemoveIter])
            word[postRemoveIter++] = word[preRemoveIter];

    word.resize(postRemoveIter);
    return word;
}

template<typename T>
std::vector<index_t> &PairCompressor<T>::compressFilteredPairs()
{
    // marks positions in the word which will be erased after compression
    std::vector<bool> toRemoveMask(word.size());

    index_t emptyLetter = grammar.getEmptyLetter();
    indexpair_t prevPair(emptyLetter, emptyLetter);

    index_t pairSubstitute;
    for (index_t occIter = 0; occIter < pairFilter.sortedOccurrences->size();
         occIter++) {

        LetterOccurrenceContext<T> &currentOcc =
                pairFilter.sortedOccurrences->at(occIter);

        indexpair_t currentPair =
                std::make_pair(currentOcc.predecessor, currentOcc.central);
        index_t currentPairPositionInWord = currentOcc.pointer-1;

        // a new pair occurred; set a substitute (a fresh letter)
        //   which will be used for next occurrences of the same pair
        if (prevPair != currentPair) {
            pairSubstitute = grammar.newNonterminal(currentPair.first,
                                                    currentPair.second);
            prevPair = currentPair;
        }

        // replace the pair with the substitute
        word[currentPairPositionInWord] = pairSubstitute;
        toRemoveMask[currentPairPositionInWord+1] = true;
    }

    word = removePositionsFromWord(toRemoveMask);
    return word;
}

#endif // PAIRCOMPRESSOR_H
