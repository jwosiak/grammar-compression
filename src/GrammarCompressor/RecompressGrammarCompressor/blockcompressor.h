#ifndef BLOCKCOMPRESSOR_H
#define BLOCKCOMPRESSOR_H

#include <vector>
#include "types.h"
#include "blocktogrammardecomposer.h"

class Grammar;
class BlockOfLetters;

class BlockCompressor
{
    std::vector<index_t> &word;
    Grammar &grammar;

    // decomposition of blocks strategy
    BlockToGrammarDecomposer blockDecomposer;
public:
    BlockCompressor(std::vector<index_t> &word, Grammar &grammar);

    std::vector<index_t> &compressBlocks();

private:
    bool isNextBlockStarting(
            BlockOfLetters &currentBlock, index_t newlyEncounteredLetter);
    std::vector<BlockOfLetters> &finishProcessingBlock(
            BlockOfLetters &currentBlock, std::vector<BlockOfLetters> &blocks);
    std::vector<BlockOfLetters> getBlocksFromWord();

    std::vector<index_t> &replaceBlocksWithLetters(
            std::vector<BlockOfLetters> &blocks);
    std::vector<BlockOfLetters> lexSortBlocks(
            std::vector<BlockOfLetters> &blocks);
    Grammar &addToGrammarRulesProducingBlocks(
            std::vector<BlockOfLetters> &blocks);
    std::vector<index_t> &compressBlocks(std::vector<BlockOfLetters> &blocks);

};

#endif // BLOCKCOMPRESSOR_H
