#ifndef ALPHABETPARTITION_H
#define ALPHABETPARTITION_H

#include "types.h"
#include <vector>

struct AlphabetPartition
{
    // used to indicate left/right partition
    enum side {
        LEFT = 0,
        RIGHT = 1
    };

    // two sets (represented as masks) of letters from the alphabet
    std::vector<bool> subpartitionsMasks[2];

    // whole alphabet consists of values (letters) from [0, alphabetMaxLetter]
    const index_t alphabetMaxLetter;

    AlphabetPartition(index_t alphabetMaxLetter);

    bool pairFitsPartition(index_t leftLetter, index_t rightLetter) const;
    bool pairFitsReversedPartition(
            index_t leftLetter, index_t rightLetter) const;
    void swapSubpartitions();
};

#endif // ALPHABETPARTITION_H
