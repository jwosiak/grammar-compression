#include "blocktogrammardecomposer.h"
#include "grammar.h"
#include <cmath>

BlockToGrammarDecomposer::BlockToGrammarDecomposer(Grammar &grammar)
    : grammar(grammar)
{

}

void BlockToGrammarDecomposer::reset(index_t blockBaseLetter)
{
    this->blockBaseLetter = blockBaseLetter;

    alreadyDecomposedBlocks.clear();
    alreadyDecomposedBlocks[1] = blockBaseLetter;

    maxOfAlreadyDecomposedBlocks = 1;
}

index_t BlockToGrammarDecomposer::decomposeNext(index_t blockLength)
{
    if (isAlreadyDecomposed(blockLength))
        return alreadyDecomposedBlocks[blockLength];

    // build left subrule        
    index_t leftLength;
    if (blockLength > maxOfAlreadyDecomposedBlocks)
        leftLength = blockLength - maxOfAlreadyDecomposedBlocks;
    else
        leftLength = calculateHighestPowerOfTwoNotGreaterThan(blockLength);

    maxOfAlreadyDecomposedBlocks =
            std::max(maxOfAlreadyDecomposedBlocks, blockLength);

    index_t leftProd = decomposeNext(leftLength);

    // build right subrule
    index_t rightLength = blockLength - leftLength;
    index_t rightProd = decomposeNext(rightLength);

    // build main rule
    index_t mainProd = grammar.newNonterminal(leftProd, rightProd);
    alreadyDecomposedBlocks[blockLength] = mainProd;

    return mainProd;
}

bool BlockToGrammarDecomposer::isAlreadyDecomposed(index_t blockLength)
{
    return (blockLength == 1) || (alreadyDecomposedBlocks[blockLength] != 0);
}

index_t BlockToGrammarDecomposer::calculateHighestPowerOfTwoNotGreaterThan(
        index_t number)
{
    static const unsigned int twos[] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512,
                                         1024, 2048, 4096, 8192, 16384, 32768,
                                         65536, 131072, 262144, 524288,
                                         1048576, 2097152, 4194304, 8388608,
                                         16777216, 33554432, 67108864,
                                         134217728, 268435456, 536870912,
                                         1073741824, 2147483648 };

    index_t floorOfLog = std::floor(std::log2(number-1));
    return twos[floorOfLog];
}
