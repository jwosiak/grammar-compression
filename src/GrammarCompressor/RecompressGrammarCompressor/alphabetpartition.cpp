#include "alphabetpartition.h"

AlphabetPartition::AlphabetPartition(index_t alphabetMaxLetter)
    : alphabetMaxLetter(alphabetMaxLetter)
{
    for (int i = 0; i < 2; i++)
        subpartitionsMasks[i] = std::vector<bool>(alphabetMaxLetter+1);
}

bool AlphabetPartition::pairFitsPartition(
        index_t leftLetter, index_t rightLetter) const
{
    return subpartitionsMasks[LEFT][leftLetter]
            && subpartitionsMasks[RIGHT][rightLetter];
}

bool AlphabetPartition::pairFitsReversedPartition(
        index_t leftLetter, index_t rightLetter) const
{
    return subpartitionsMasks[RIGHT][leftLetter]
            && subpartitionsMasks[LEFT][rightLetter];
}

void AlphabetPartition::swapSubpartitions()
{
    std::swap(subpartitionsMasks[0], subpartitionsMasks[1]);
}
