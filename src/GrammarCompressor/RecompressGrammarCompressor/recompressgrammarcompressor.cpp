#include "recompressgrammarcompressor.h"
#include <cmath>
#include "blockcompressor.h"
#include "paircompressor.h"

RecompressGrammarCompressor::RecompressGrammarCompressor(
        std::vector<terminal_t> &word)
{
    this->word.reserve(word.size());
    for (index_t i = 0; i < word.size(); i++)
        this->word.push_back(word[i]);
    word.clear();
}

void RecompressGrammarCompressor::constructGrammar()
{
    while (word.size() > 1) {
        // a block is c^k, where c is a letter and k >= 2
        BlockCompressor blockCompressor(word, grammar);
        word = blockCompressor.compressBlocks();

        // find optimal number of bits to represent a letter of alphabet
        //   to space efficiently compress pairs
        switch (calculateNumberOfBitsToRepresentAlphabet()) {
        case RequiredBits::EIGHT:
            word = PairCompressor<uint8_t>(word, grammar).compressPairs();
            break;
        case RequiredBits::SIXTEEN:
            word = PairCompressor<uint16_t>(word, grammar).compressPairs();
            break;
        default:
            word = PairCompressor<uint32_t>(word, grammar).compressPairs();
        }
    }
}

Grammar &RecompressGrammarCompressor::getGrammar()
{
    return grammar;
}

RecompressGrammarCompressor::RequiredBits
    RecompressGrammarCompressor::calculateNumberOfBitsToRepresentAlphabet()
{
    // grammar size + 1 (the special character, used in pair compression)
    index_t alphabetSize = grammar.size() + 1;
    index_t numOfBits = index_t(std::ceil(std::log2(alphabetSize)));

    if (numOfBits <= 8)
        return RequiredBits::EIGHT;

    if (numOfBits <= 16)
        return RequiredBits::SIXTEEN;

    return RequiredBits::THIRTY_TWO;
}
