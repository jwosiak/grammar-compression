#ifndef REALLYSIMPLEGRAMMARCOMPRESSOR_H
#define REALLYSIMPLEGRAMMARCOMPRESSOR_H

#include "grammarcompressor.h"
#include "types.h"
#include "grammar.h"
#include <vector>

class Factor;

class ReallySimpleGrammarCompressor : public GrammarCompressor
{
    std::vector<index_t> word;

    // it matches the interface of LZCompressor, the word doesn't;
    // lz factorization is done only at the beginning of compression
    std::vector<terminal_t> &initialWord;

    Grammar grammar;

    std::vector<index_t> start;
    const index_t startFalseValue;
    std::vector<bool> end;
    std::vector<index_t> newpos;

    enum LettersPairing
    {
        NONE,
        FIRST,
        SECOND,
    };

    std::vector<LettersPairing> pairs;
public:
    ReallySimpleGrammarCompressor(std::vector<terminal_t> &word);

    // GrammarCompressor interface
    virtual void constructGrammar();
    virtual Grammar &getGrammar();

private:
    void initWord(const std::vector<terminal_t> &vec);
    void assignFactorStartAndEnd(const Factor &factor, index_t factorPos);
    void initArrayFields();

    bool factorStartsAt(index_t i);
    void clearFactorBeginningAt(index_t i);
    void splitFactorBeginningAt(index_t i);
    void splitFactorEndAt(index_t i);

    bool isSingleLetterFactorAt(index_t i);
    bool isSelfRefNotPairableFactorAt(index_t i);
    void removeSingleLetterFactorAt(index_t i);
    void shortenSelfRefFactorAt(index_t i);
    void preproc();

    bool isBadlyPairedDefinitionOfFactorAt(index_t i);
    bool isFreeLetterAt(index_t i);
    bool isNotPairedAt(index_t i);
    void pairSubsequentLettersAt(index_t i);
    void copyPairingFromDefinitionToFactorAt(index_t &i);
    bool isOddLengthFactor(index_t factorEndPos);
    void splitAndUnpairFactorEnd(index_t factorEndPos);
    void pairLettersOfFactorAt(index_t &i);
    void pairFreeLetter(index_t i);
    void pairing();

    void passUnpairedFreeLetter(index_t &sourcePos, index_t &destPos);
    void replacePairOfSubsequentFreeLetters(
            index_t &sourcePos, index_t &destPos);
    void reassignFactorBeginning(index_t &sourcePos, index_t &destPos);
    void replacePairInsideFactor(
            index_t &sourcePos, index_t &destPos, index_t &destDefinitionPos);
    void reassignFactorEnd(index_t sourcePos, index_t destPos);
    void replacePairsInFactor(index_t &sourcePos, index_t &destPos);
    void resizeArrayFields(index_t newSize);
    void pairReplacement();
};

#endif // REALLYSIMPLEGRAMMARCOMPRESSOR_H
