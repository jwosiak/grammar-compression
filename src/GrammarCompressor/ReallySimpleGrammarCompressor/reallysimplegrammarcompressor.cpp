#include "reallysimplegrammarcompressor.h"
#include "lzcompressor.h"
#include "factor.h"

ReallySimpleGrammarCompressor::ReallySimpleGrammarCompressor(
        std::vector<terminal_t> &word)
    : initialWord(word)
    , startFalseValue(word.size())
{

}

void ReallySimpleGrammarCompressor::constructGrammar()
{
    initArrayFields();
    while (word.size() > 1) {
        preproc();
        pairing();
        pairReplacement();
    }
}

Grammar &ReallySimpleGrammarCompressor::getGrammar()
{
    return grammar;
}

void ReallySimpleGrammarCompressor::initWord(
        const std::vector<terminal_t> &vec)
{
    word.reserve(vec.size());
    for (index_t i = 0; i < vec.size(); i++)
        word.push_back(vec[i]);
}

void ReallySimpleGrammarCompressor::assignFactorStartAndEnd(
        const Factor &factor, index_t factorPos)
{
    // factorPos is the position where the factor occurrs
    // factor.position is the position of the factor's definition
    if (factor.length > 1) {
        start[factorPos] = factor.position;
        end[factorPos + factor.length - 1] = true;
    }
}

void ReallySimpleGrammarCompressor::initArrayFields()
{
    // memory efficient order of operations

    std::vector<Factor> factorization = LZCompressor::selfRef(initialWord);
    initWord(initialWord);
    initialWord.clear();

    start = std::vector<index_t>(word.size(), startFalseValue);
    end.resize(word.size());

    index_t factorBeginning = 0;
    for (const Factor &factor : factorization) {
        assignFactorStartAndEnd(factor, factorBeginning);
        factorBeginning += std::max(index_t(1), factor.length);
    }
    factorization.clear();

    newpos.resize(word.size());
    pairs.resize(word.size());
}

bool ReallySimpleGrammarCompressor::factorStartsAt(index_t i)
{
    return (i < word.size()) && (start[i] != startFalseValue);
}

void ReallySimpleGrammarCompressor::clearFactorBeginningAt(index_t i)
{
    start[i] = startFalseValue;
}

void ReallySimpleGrammarCompressor::splitFactorBeginningAt(index_t i)
{
    if (end[i+1]) {
        // splitted 2-length factor is no longer a factor
        clearFactorBeginningAt(i);
        end[i+1] = false;
    } else {
        // removes first letter from a factor
        start[i+1] = start[i] + 1;
        clearFactorBeginningAt(i);
    }
}

void ReallySimpleGrammarCompressor::splitFactorEndAt(index_t i)
{
    if (factorStartsAt(i-1)) {
        // splitted 2-length factor is no longer a factor
        clearFactorBeginningAt(i-1);
        end[i] = false;
    } else {
        // removes last letter from a factor
        end[i-1] = true;
        end[i] = false;
    }
}

bool ReallySimpleGrammarCompressor::isSingleLetterFactorAt(index_t i)
{
    // true if a single letter is marked as a factor
    return factorStartsAt(i) && end[i];
}

bool ReallySimpleGrammarCompressor::isSelfRefNotPairableFactorAt(index_t i)
{
    // a factor is can't be properly paired, when its second position
    //   references to the first
    return start[i] == (i - 1);
}

void ReallySimpleGrammarCompressor::removeSingleLetterFactorAt(index_t i)
{
    // a single letter is no longer marked as a factor
    clearFactorBeginningAt(i);
    end[i] = false;
}

void ReallySimpleGrammarCompressor::shortenSelfRefFactorAt(index_t i)
{   
    splitFactorBeginningAt(i);
    if (factorStartsAt(i+1))
        start[i+1] = i - 1;
}

void ReallySimpleGrammarCompressor::preproc()
{
    std::fill(pairs.begin(), pairs.end(), NONE);

    // removes all single letters marked as factors
    // makes some unpairable self-referential factors pairable
    for (index_t i = 0; i < word.size(); i++) {
        if (isSingleLetterFactorAt(i))
            removeSingleLetterFactorAt(i);

        if (isSelfRefNotPairableFactorAt(i))
            shortenSelfRefFactorAt(i);
    }
}

bool ReallySimpleGrammarCompressor::isBadlyPairedDefinitionOfFactorAt(
        index_t i)
{
    return factorStartsAt(i)
            && (((pairs[start[i]] == NONE) && (pairs[i-1] == NONE))
            || pairs[start[i]] == SECOND);
}

bool ReallySimpleGrammarCompressor::isFreeLetterAt(index_t i)
{
    // not exaclty checks if a letter is a free one
    //   but the function is used only in a proper context,
    //   when it is the sufficient condition
    return !(factorStartsAt(i) || end[i]);
}

bool ReallySimpleGrammarCompressor::isNotPairedAt(index_t i)
{
    return pairs[i] == NONE;
}

void ReallySimpleGrammarCompressor::pairSubsequentLettersAt(index_t i)
{
    pairs[i] = FIRST;
    pairs[i+1] = SECOND;
}

void ReallySimpleGrammarCompressor::copyPairingFromDefinitionToFactorAt(
        index_t &i)
{
    index_t j = start[i];
    do {
        pairs[i++] = pairs[j++];
    } while (!end[i-1]);

    i--;
}

bool ReallySimpleGrammarCompressor::isOddLengthFactor(index_t factorEndPos)
{
    return pairs[factorEndPos] == FIRST;
}

void ReallySimpleGrammarCompressor::splitAndUnpairFactorEnd(
        index_t factorEndPos)
{
    splitFactorEndAt(factorEndPos);
    pairs[factorEndPos] = NONE;
}

void ReallySimpleGrammarCompressor::pairLettersOfFactorAt(index_t &i)
{
    copyPairingFromDefinitionToFactorAt(i);
    // now 'i' points at the end of the factor

    // factor's length must be even; remove last letter if it's not
    if (isOddLengthFactor(i))
        splitAndUnpairFactorEnd(i);
}

void ReallySimpleGrammarCompressor::pairFreeLetter(index_t i)
{
    if (!isFreeLetterAt(i-1))
        splitFactorEndAt(i-1);
    pairSubsequentLettersAt(i-1);
}

void ReallySimpleGrammarCompressor::pairing()
{
    for (index_t i = 1; i < word.size(); i++) {
        if (isBadlyPairedDefinitionOfFactorAt(i))
            splitFactorBeginningAt(i);

        if (factorStartsAt(i))
            pairLettersOfFactorAt(i);
            // now 'i' points at the end of the factor

        if (isFreeLetterAt(i) && isNotPairedAt(i-1))
            pairFreeLetter(i);
    }
}

void ReallySimpleGrammarCompressor::passUnpairedFreeLetter(
        index_t &sourcePos, index_t &destPos)
{
    word[destPos] = word[sourcePos];
    newpos[sourcePos++] = destPos++;
}

void ReallySimpleGrammarCompressor::replacePairOfSubsequentFreeLetters(
        index_t &sourcePos, index_t &destPos)
{
    word[destPos] = grammar.newNonterminal(word[sourcePos], word[sourcePos+1]);
    newpos[sourcePos] = newpos[sourcePos+1] = destPos;
    sourcePos += 2;
    destPos++;
}

void ReallySimpleGrammarCompressor::reassignFactorBeginning(
        index_t &sourcePos, index_t &destPos)
{
    index_t destDefinitionPos = newpos[start[sourcePos]];
    start[destPos] = destDefinitionPos;
    clearFactorBeginningAt(sourcePos);
}

void ReallySimpleGrammarCompressor::replacePairInsideFactor(
        index_t &sourcePos, index_t &destPos, index_t &destDefinitionPos)
{
    word[destPos] = word[destDefinitionPos];
    newpos[sourcePos++] = destPos;
    if ((sourcePos < word.size()) && (pairs[sourcePos] == SECOND))
        newpos[sourcePos++] = destPos;
    destPos++;
    destDefinitionPos++;
}

void ReallySimpleGrammarCompressor::reassignFactorEnd(
        index_t sourcePos, index_t destPos)
{
    end[destPos] = true;
    end[sourcePos] = false;
}

void ReallySimpleGrammarCompressor::replacePairsInFactor(
        index_t &sourcePos, index_t &destPos)
{
    // replaces paired letters in the factor with single letters

    reassignFactorBeginning(sourcePos, destPos);
    index_t destDefinitionPos = start[destPos];

    do {
        replacePairInsideFactor(sourcePos, destPos, destDefinitionPos);
    } while (!end[sourcePos-1]);

    reassignFactorEnd(sourcePos-1, destPos-1);
}

void ReallySimpleGrammarCompressor::resizeArrayFields(index_t newSize)
{
    word.resize(newSize);
    end.resize(newSize);
    start.resize(newSize);
    pairs.resize(newSize);
}

void ReallySimpleGrammarCompressor::pairReplacement()
{
    index_t preReplacementIter = 0;
    index_t postReplacementIter = 0;

    // all functions, except predicates, change iterators values
    while (preReplacementIter < word.size()) {
        if (isFreeLetterAt(preReplacementIter)) {
            if (isNotPairedAt(preReplacementIter))
                passUnpairedFreeLetter(preReplacementIter, postReplacementIter);
            else
                replacePairOfSubsequentFreeLetters(preReplacementIter,
                                                   postReplacementIter);
        }

        if (factorStartsAt(preReplacementIter))
            replacePairsInFactor(preReplacementIter, postReplacementIter);
    }

    resizeArrayFields(postReplacementIter);
}
