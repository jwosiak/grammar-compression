#include "gentools.h"

#include <QDebug>
#include <cmath>
#include <algorithm>
#include <climits>

std::vector<terminal_t> Tools::randText(terminal_t letterLength, index_t size)
{
    std::vector<terminal_t> result;
    result.reserve(size);
    for (index_t i = 0; i < size; i++) {
        result.push_back(std::rand() % (1 << letterLength));
    }
    return result;
}

std::vector<terminal_t> Tools::genTextWithManyBlocks(
        index_t n, index_t blockSize)
{
    std::vector<terminal_t> res;
    res.reserve(n);
    index_t counter = 0;
    terminal_t letter = 0;
    for (index_t i = 0; i < n; i++) {
        if (counter == blockSize) {
            counter = 0;
            letter++;
        }
        res.push_back(letter);
        counter++;
    }
    return res;
}

std::vector<terminal_t> Tools::genIncreasingLengthBlocks(index_t n)
{
    std::vector<terminal_t> res;
    res.reserve(n);
    index_t counter = 0;
    index_t nextBlockLength = 3;
    terminal_t letter = 0;
    for (index_t i = 0; i < n; i++) {
        if (counter == nextBlockLength) {
            counter = 0;
            letter = 1 - letter;
            if (letter == 0)
                nextBlockLength += 2;
        }
        res.push_back(letter);
        counter++;
    }
    return res;
}

void Tools::fibonacciWordRecPart(
        index_t n, index_t pos, std::vector<index_t> &startedAt,
        std::vector<index_t> &lengths, std::vector<terminal_t> &result)
{
    index_t len = lengths[n];
    if (len > 0) {
        index_t beginning = startedAt[n];
        for (index_t j = beginning; j < beginning + len; j++)
            result.push_back(result[j]);
        return;
    }

    if (n == 0) {
        result.push_back(0);
        startedAt[0] = pos;
        lengths[0] = 1;
        return;
    }

    if (n == 1) {
        result.push_back(0);
        result.push_back(1);
        startedAt[1] = pos;
        lengths[1] = 2;
        return;
    }

    fibonacciWordRecPart(n-1, pos, startedAt, lengths, result);

    fibonacciWordRecPart(n-2, pos+lengths[n-1], startedAt, lengths, result);

    startedAt[n] = pos;
    lengths[n] = lengths[n-1] + lengths[n-2];
}

std::vector<terminal_t> Tools::fibonacciWord(index_t n)
{

   std::vector<index_t> startedAt(n+1);
   std::vector<index_t> lengths(n+1);
   std::vector<terminal_t> result;
   result.reserve(2*n);

   fibonacciWordRecPart(n, 0, startedAt, lengths, result);

   return result;
}
