#ifndef GENTOOLS_H
#define GENTOOLS_H

#ifndef TOOLS_H
#define TOOLS_H

#include "types.h"
#include <utility>

namespace Tools
{

    std::vector<terminal_t> randText(terminal_t letterLength, index_t size);
    std::vector<terminal_t> genTextWithManyBlocks(index_t n, index_t blockSize);
    std::vector<terminal_t> genIncreasingLengthBlocks(index_t n);
    void fibonacciWordRecPart(index_t n, index_t pos,
                              std::vector<index_t> &startedAt, std::vector<index_t> &lengths,
                              std::vector<terminal_t> &result);
    std::vector<terminal_t> fibonacciWord(index_t n);

    template <typename T>
    bool compareWords(const std::vector<T> &v1, const std::vector<T> &v2)
    {
        return (v1.size() == v2.size())
                && std::equal(v1.begin(), v1.end(), v2.begin());
    }
};

#endif // TOOLS_H

#endif // GENTOOLS_H
