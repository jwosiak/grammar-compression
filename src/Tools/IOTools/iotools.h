#ifndef IOTOOLS_H
#define IOTOOLS_H

#include <vector>
#include "types.h"
#include <QString>

class Grammar;

namespace Tools
{
    std::vector<terminal_t> readFile(QString filePath);
    void writeToFile(QString filePath, std::vector<terminal_t> &v);
    bool compareFiles(QString filePath1, QString filePath2);
    void writeStatsToCSV(QString csvPath, vectormap &st);
    void writeGeneralStatsToJSON(
            QString jsonPath, unsigned int compressionTime, index_t textLength,
            index_t maxHeight, index_t compressedSize);
    void writeGrammarToFile(const Grammar &gr, QString outputPath);
    Grammar readGrammar(QString inputPath);
}

#endif // IOTOOLS_H
