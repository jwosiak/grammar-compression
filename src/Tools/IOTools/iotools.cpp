#include "iotools.h"
#include "grammar.h"

#include <iostream>
#include <fstream>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>


std::vector<terminal_t> Tools::readFile(QString filePath)
{
    std::ifstream file(filePath.toUtf8());
    std::vector<terminal_t> res;

    while (file.good()) {
      res.push_back(static_cast<terminal_t>(file.get()));
    }
    res.pop_back();

    file.close();
    return res;
}

void Tools::writeToFile(QString filePath, std::vector<terminal_t> &v)
{
    std::ofstream file(filePath.toUtf8(), std::ofstream::trunc);

    for (unsigned int i = 0; i < v.size(); i++) {
        file.put(static_cast<char>(v[i]));
    }

    file.close();
}

bool Tools::compareFiles(QString filePath1, QString filePath2)
{
    std::ifstream file1(filePath1.toUtf8());
    std::ifstream file2(filePath2.toUtf8());

    while (file1.good() && file2.good()) {
        if (file1.get() != file2.get()) {
            file1.close();
            file2.close();
            return false;
        }
    }

    bool res = !file1.good() && !file2.good();
    file1.close();
    file2.close();
    return res;
}

void Tools::writeStatsToCSV(QString csvPath, vectormap &st)
{
    std::ofstream csvFile(csvPath.toUtf8());

    csvFile << "Length, Count, Height\n";

    std::vector<index_t> *lengths = st["Length"];
    std::vector<index_t> *counts = st["Count"];
    std::vector<index_t> *heights = st["Height"];

    for (index_t i = 0; i < lengths->size(); i++) {
        csvFile << QString("%1,%2,%3\n")
                   .arg((*lengths)[i]).arg((*counts)[i]).arg((*heights)[i])
                   .toUtf8().toStdString();
    }

    csvFile.close();
}

void Tools::writeGeneralStatsToJSON(
        QString jsonPath, unsigned int compressionTime, index_t textLength,
        index_t maxHeight, index_t compressedSize)
{
    QJsonDocument root;
    QJsonObject stats;

    stats.insert("Max height", qint64(maxHeight));
    stats.insert("Compression time (ms)", qint64(compressionTime));
    stats.insert("Size", qint64(compressedSize));
    stats.insert("Input text length", qint64(textLength));
    root.setObject(stats);

    std::ofstream jsonFile(jsonPath.toUtf8());
    jsonFile << root.toJson().toStdString();

    jsonFile.close();
}

void Tools::writeGrammarToFile(const Grammar &gr, QString outputPath)
{
    std::ofstream file(outputPath.toUtf8(), std::ofstream::out
                       | std::ofstream::binary | std::ofstream::trunc);

    index_t nonterminalsSize = gr.getProductions().size();
    file.write(reinterpret_cast<const char *>(&nonterminalsSize), 4);

    for (const indexpair_t &X : gr.getProductions()) {
        file.write(reinterpret_cast<const char *>(&X.first), 4);
        file.write(reinterpret_cast<const char *>(&X.second), 4);
    }

    file.close();
}

Grammar Tools::readGrammar(QString inputPath)
{
    std::ifstream file(inputPath.toUtf8(),
                       std::ifstream::in | std::ifstream::binary);
    char buffer[4];
    file.read(buffer, 4);
    index_t nonterminalsSize = *reinterpret_cast<index_t*>(buffer);

    std::vector<production_t> nonterminals;
    for (index_t i = 0; i < nonterminalsSize; i++) {
        indexpair_t X;
        file.read(buffer, 4);
        X.first = *reinterpret_cast<index_t*>(buffer);
        file.read(buffer, 4);
        X.second= *reinterpret_cast<index_t*>(buffer);
        nonterminals.push_back(X);
    }

    file.close();
    return Grammar(nonterminals);
}
