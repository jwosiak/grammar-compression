#ifndef TIMER_H
#define TIMER_H

#include <QString>

namespace Tools
{
    void pushTimer(const QString msg="");
    unsigned int popTimer();
};

#endif // TIMER_H
