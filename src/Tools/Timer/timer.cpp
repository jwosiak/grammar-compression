#include "timer.h"
#include <chrono>
#include <stack>
#include <QtDebug>

using namespace std::chrono;

static std::stack<std::pair<QString, long> > timer;

void Tools::pushTimer(const QString msg)
{
    long t0 = duration_cast<milliseconds>(
                system_clock::now().time_since_epoch()).count();

    timer.push(std::pair<QString, long>(msg, t0));
}

unsigned int Tools::popTimer()
{
    std::pair<QString, long> top = timer.top();
    timer.pop();

    QString msg = top.first;
    long t0 = top.second;
    long t = duration_cast<milliseconds>(
                system_clock::now().time_since_epoch()).count();
    if (msg.size() > 0)
        qDebug() << (msg + " %1").arg((t - t0) / 1000.);
    return t - t0;
}
