#ifndef TYPES_H
#define TYPES_H

#include <cinttypes>
#include <QString>
#include <map>
#include <vector>
#include <utility>

typedef uint8_t terminal_t;
typedef uint32_t index_t;
typedef std::pair<index_t, index_t> production_t;
typedef std::pair<index_t, index_t> indexpair_t;
typedef indexpair_t factor_t;

typedef std::map<QString, std::vector<index_t>*> vectormap;


#endif // TYPES_H
