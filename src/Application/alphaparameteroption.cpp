#include "alphaparameteroption.h"
#include <string>

AlphaParameterOption::AlphaParameterOption()
    : Option("--alpha", "0.292")
{

}

QString AlphaParameterOption::getUsageInfo()
{
    return Option::getUsageInfo() + "The parameter of the Alpha algorithm.";
}

QString AlphaParameterOption::getAvailableParameterInfo()
{
    return Option::getAvailableParameterInfo()
            + "A real number from range (0, 0.292]. Default: 0.292.";
}

QString AlphaParameterOption::getInvalidParameterInfo()
{
    return QString("'%1' is not an available value of alpha parameter.")
            .arg(parameter);
}

bool AlphaParameterOption::isParameterValid()
{
    float valueOfParameter;
    try {
        valueOfParameter = std::stof(parameter.toStdString());
    } catch (std::exception exc) {
        return false;
    }
    return (0 < valueOfParameter) && (valueOfParameter <= 0.292);
}
