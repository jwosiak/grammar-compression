#include <QString>
#include <vector>
#include <iostream>

#include "grammar.h"
#include "iotools.h"
#include "types.h"
#include "timer.h"
#include "factor.h"

#include "grammarcompressor.h"
#include "avlbasedgrammarcompressor.h"
#include "alphabalancedgrammarcompressor.h"
#include "recompressgrammarcompressor.h"
#include "reallysimplegrammarcompressor.h"
#include "lzcompressor.h"

#include "programargumentsparser.h"
#include "parseerrorexception.h"
#include "statsgenerator.h"

int main(int argc, char *argv[])
{
    ProgramArgumentsParser optionsParser;
    QString inputPath;
    // parse program's options and the argument
    try {
        optionsParser.parseOptions(argc, argv);

        // if a user wants help, then write only help and return
        if (optionsParser.isHelpToBeWritten()) {
            std::cout << optionsParser.getHelpContent().toStdString()
                      << '\n';
            return 0;
        }

        inputPath = optionsParser.parseAndGetArgument(argc, argv);
    } catch (ParseErrorException exc) {
        std::cerr << exc.what() << "\n";
        return -1;
    }

    QString outputPath = optionsParser.getOptionParam("--out");

    if (optionsParser.isOptionSetTrue("--dec")) {
        // decompress grammar mode
        Grammar grammar = Tools::readGrammar(inputPath);
        std::vector<terminal_t> grammarProduction = grammar.produceWord();
        Tools::writeToFile(outputPath + "grammar_production",
                           grammarProduction);
        return 0;
    }

    std::vector<terminal_t> inputWord = Tools::readFile(inputPath);
    index_t inputWordSize = inputWord.size();

    GrammarCompressor *compressor = nullptr;
    std::vector<Factor> factorization;

    QString compressorAlgorithm =
            optionsParser.getOptionParam("--compr");

    if (compressorAlgorithm == "AVL")
        compressor = new AvlBasedGrammarCompressor(inputWord);
    if (compressorAlgorithm == "Alpha") {
        float alpha = optionsParser.getAlphaParameter();
        compressor = new AlphaBalancedGrammarCompressor(inputWord, alpha);
    }
    if (compressorAlgorithm == "Recompress")
        compressor = new RecompressGrammarCompressor(inputWord);
    if (compressorAlgorithm == "Simple")
        compressor = new ReallySimpleGrammarCompressor(inputWord);

    Tools::pushTimer("");
    // perform compression
    if (inputWordSize > 0)
        if (compressor != nullptr) {
            compressor->constructGrammar();
        } else {
            if (compressorAlgorithm == "LZ1")
                factorization = LZCompressor::noSelfRef(inputWord);
            if (compressorAlgorithm == "LZ77")
                factorization = LZCompressor::selfRef(inputWord);
        }
    unsigned int compressionTime = Tools::popTimer();


    if (optionsParser.isOptionSetTrue("--grdef") && (compressor != nullptr))
        Tools::writeGrammarToFile(compressor->getGrammar(),
                                  outputPath + "/grammar");

    StatsGenerator statsGenerator(outputPath);
    statsGenerator.setInputWordSize(inputWordSize);
    statsGenerator.setCompressionTime(compressionTime);

    // write stats to files
    if (optionsParser.isOptionSetTrue("--gstat")
            || optionsParser.isOptionSetTrue("--dstat"))
        if (compressor != nullptr) {
            // clear unnecesary data to save memory
            compressor->getGrammar().clearCache();
            statsGenerator.computeStatsForGrammar(compressor->getGrammar());
            if (optionsParser.isOptionSetTrue("--dstat"))
                statsGenerator.writeGrammarDetailedStats();
            if (optionsParser.isOptionSetTrue("--gstat"))
                statsGenerator.writeGrammarGeneralStats();
        } else if (optionsParser.isOptionSetTrue("--gstat")) {
            statsGenerator.writeFactorizationGeneralStats(factorization);
        }

    if (compressor != nullptr)
        delete compressor;
    return 0;
}
