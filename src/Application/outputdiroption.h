#ifndef OUTPUTDIROPTION_H
#define OUTPUTDIROPTION_H

#include "option.h"

class OutputDirOption : public Option
{
public:
    OutputDirOption();

    QString getUsageInfo();
    QString getAvailableParameterInfo();
    bool isParameterValid();
    QString getInvalidParameterInfo();
};

#endif // OUTPUTDIROPTION_H
