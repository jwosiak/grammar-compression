#ifndef PARSEERROREXCEPTION_H
#define PARSEERROREXCEPTION_H

#include <exception>
#include <QString>
#include <string>

class ParseErrorException : public std::exception
{
    std::string message;
public:
    ParseErrorException(QString message);

    const char *what() const noexcept;
};

#endif // PARSEERROREXCEPTION_H
