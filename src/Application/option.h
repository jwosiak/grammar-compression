#ifndef OPTION_H
#define OPTION_H

#include <QString>

class Option
{
protected:
    const QString name;
    QString parameter;

    Option(QString name, QString defaultParameter);
public:
    virtual ~Option();

    bool hasName(QString name);
    virtual QString getUsageInfo();
    virtual QString getAvailableParameterInfo();
    virtual QString getInvalidParameterInfo() = 0;
    virtual bool isParameterValid() = 0;
    void setParameter(QString parameter);
    QString getParameter() const;
    QString getName() const;
};

#endif // OPTION_H
