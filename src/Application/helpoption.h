#ifndef HELPOPTION_H
#define HELPOPTION_H

#include "option.h"
#include <vector>

class HelpOption : public Option
{
    std::vector<Option*> optionsToHelp;
public:
    HelpOption(std::vector<Option*> &optionsToHelp);

    QString getUsageInfo();
    QString getInvalidParameterInfo();
    bool isParameterValid();
};

#endif // HELPOPTION_H
