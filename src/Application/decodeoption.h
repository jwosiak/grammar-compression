#ifndef DECODEOPTION_H
#define DECODEOPTION_H

#include "booleanvalueoption.h"

class DecodeOption : public BooleanValueOption
{
public:
    DecodeOption();

    QString getUsageInfo();
    QString getAvailableParameterInfo();
};

#endif // DECODEOPTION_H
