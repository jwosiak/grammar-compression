#include "writegeneralstatsoption.h"

WriteGeneralStatsOption::WriteGeneralStatsOption()
    : BooleanValueOption("--gstat", "F")
{

}

QString WriteGeneralStatsOption::getUsageInfo()
{
    return Option::getUsageInfo()
            + "If set, then the program outputs grammar general stats.";
}

QString WriteGeneralStatsOption::getAvailableParameterInfo()
{
    return BooleanValueOption::getAvailableParameterInfo()
            + "Default: F.";
}
