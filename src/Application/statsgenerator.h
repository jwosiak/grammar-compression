#ifndef STATSGENERATOR_H
#define STATSGENERATOR_H

#include <QString>
#include <vector>
#include "types.h"

class Grammar;
class Factor;

class StatsGenerator
{
    const QString outputPath;
    unsigned int compressionTime;
    index_t inputWordSize;
    vectormap nonterminalsDetailedStats;
public:
    StatsGenerator(QString outputPath);
    ~StatsGenerator();

    void computeStatsForGrammar(const Grammar &grammar);
    void writeGrammarDetailedStats();
    void writeGrammarGeneralStats();
    void writeFactorizationGeneralStats(
            const std::vector<Factor> &factorization);
    void setCompressionTime(unsigned int value);
    void setInputWordSize(const index_t &value);
};

#endif // STATSGENERATOR_H
