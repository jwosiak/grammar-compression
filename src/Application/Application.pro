QT -= gui

CONFIG += c++11
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    alphaparameteroption.cpp \
    booleanvalueoption.cpp \
    compressoroption.cpp \
    decodeoption.cpp \
    helpoption.cpp \
    main.cpp \
    option.cpp \
    outputdiroption.cpp \
    parseerrorexception.cpp \
    programargumentsparser.cpp \
    statsgenerator.cpp \
    writedetailedstatsoption.cpp \
    writegeneralstatsoption.cpp \
    writeoutputgrammaroption.cpp

HEADERS += \
    alphaparameteroption.h \
    booleanvalueoption.h \
    compressoroption.h \
    decodeoption.h \
    helpoption.h \
    option.h \
    outputdiroption.h \
    parseerrorexception.h \
    programargumentsparser.h \
    statsgenerator.h \
    writedetailedstatsoption.h \
    writegeneralstatsoption.h \
    writeoutputgrammaroption.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

TARGET = $$OUT_PWD/../../GrammarCompression


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/AlphaBalancedGrammarCompressor/release/ -lAlphaBalancedGrammarCompressor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/AlphaBalancedGrammarCompressor/debug/ -lAlphaBalancedGrammarCompressor
else:unix: LIBS += -L$$OUT_PWD/../GrammarCompressor/AlphaBalancedGrammarCompressor/ -lAlphaBalancedGrammarCompressor

INCLUDEPATH += $$PWD/../GrammarCompressor/AlphaBalancedGrammarCompressor
DEPENDPATH += $$PWD/../GrammarCompressor/AlphaBalancedGrammarCompressor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/AlphaBalancedGrammarCompressor/release/libAlphaBalancedGrammarCompressor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/AlphaBalancedGrammarCompressor/debug/libAlphaBalancedGrammarCompressor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/AlphaBalancedGrammarCompressor/release/AlphaBalancedGrammarCompressor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/AlphaBalancedGrammarCompressor/debug/AlphaBalancedGrammarCompressor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/AlphaBalancedGrammarCompressor/libAlphaBalancedGrammarCompressor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/AvlBasedGrammarCompressor/release/ -lAvlBasedGrammarCompressor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/AvlBasedGrammarCompressor/debug/ -lAvlBasedGrammarCompressor
else:unix: LIBS += -L$$OUT_PWD/../GrammarCompressor/AvlBasedGrammarCompressor/ -lAvlBasedGrammarCompressor

INCLUDEPATH += $$PWD/../GrammarCompressor/AvlBasedGrammarCompressor
DEPENDPATH += $$PWD/../GrammarCompressor/AvlBasedGrammarCompressor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/AvlBasedGrammarCompressor/release/libAvlBasedGrammarCompressor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/AvlBasedGrammarCompressor/debug/libAvlBasedGrammarCompressor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/AvlBasedGrammarCompressor/release/AvlBasedGrammarCompressor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/AvlBasedGrammarCompressor/debug/AvlBasedGrammarCompressor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/AvlBasedGrammarCompressor/libAvlBasedGrammarCompressor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/ReallySimpleGrammarCompressor/release/ -lReallySimpleGrammarCompressor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/ReallySimpleGrammarCompressor/debug/ -lReallySimpleGrammarCompressor
else:unix: LIBS += -L$$OUT_PWD/../GrammarCompressor/ReallySimpleGrammarCompressor/ -lReallySimpleGrammarCompressor

INCLUDEPATH += $$PWD/../GrammarCompressor/ReallySimpleGrammarCompressor
DEPENDPATH += $$PWD/../GrammarCompressor/ReallySimpleGrammarCompressor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/ReallySimpleGrammarCompressor/release/libReallySimpleGrammarCompressor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/ReallySimpleGrammarCompressor/debug/libReallySimpleGrammarCompressor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/ReallySimpleGrammarCompressor/release/ReallySimpleGrammarCompressor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/ReallySimpleGrammarCompressor/debug/ReallySimpleGrammarCompressor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/ReallySimpleGrammarCompressor/libReallySimpleGrammarCompressor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/RecompressGrammarCompressor/release/ -lRecompressGrammarCompressor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/RecompressGrammarCompressor/debug/ -lRecompressGrammarCompressor
else:unix: LIBS += -L$$OUT_PWD/../GrammarCompressor/RecompressGrammarCompressor/ -lRecompressGrammarCompressor

INCLUDEPATH += $$PWD/../GrammarCompressor/RecompressGrammarCompressor
DEPENDPATH += $$PWD/../GrammarCompressor/RecompressGrammarCompressor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/RecompressGrammarCompressor/release/libRecompressGrammarCompressor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/RecompressGrammarCompressor/debug/libRecompressGrammarCompressor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/RecompressGrammarCompressor/release/RecompressGrammarCompressor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/RecompressGrammarCompressor/debug/RecompressGrammarCompressor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/RecompressGrammarCompressor/libRecompressGrammarCompressor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/LZCompressor/release/ -lLZCompressor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/LZCompressor/debug/ -lLZCompressor
else:unix: LIBS += -L$$OUT_PWD/../GrammarCompressor/LZCompressor/ -lLZCompressor

INCLUDEPATH += $$PWD/../GrammarCompressor/LZCompressor
DEPENDPATH += $$PWD/../GrammarCompressor/LZCompressor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/LZCompressor/release/libLZCompressor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/LZCompressor/debug/libLZCompressor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/LZCompressor/release/LZCompressor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/LZCompressor/debug/LZCompressor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/LZCompressor/libLZCompressor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/Interfaces/release/ -lInterfaces
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../GrammarCompressor/Interfaces/debug/ -lInterfaces
else:unix: LIBS += -L$$OUT_PWD/../GrammarCompressor/Interfaces/ -lInterfaces

INCLUDEPATH += $$PWD/../GrammarCompressor/Interfaces
DEPENDPATH += $$PWD/../GrammarCompressor/Interfaces

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/Interfaces/release/libInterfaces.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/Interfaces/debug/libInterfaces.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/Interfaces/release/Interfaces.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/Interfaces/debug/Interfaces.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../GrammarCompressor/Interfaces/libInterfaces.a


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Tools/IOTools/release/ -lIOTools
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Tools/IOTools/debug/ -lIOTools
else:unix: LIBS += -L$$OUT_PWD/../Tools/IOTools/ -lIOTools

INCLUDEPATH += $$PWD/../Tools/IOTools
DEPENDPATH += $$PWD/../Tools/IOTools

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/IOTools/release/libIOTools.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/IOTools/debug/libIOTools.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/IOTools/release/IOTools.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/IOTools/debug/IOTools.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../Tools/IOTools/libIOTools.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Tools/Timer/release/ -lTimer
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Tools/Timer/debug/ -lTimer
else:unix: LIBS += -L$$OUT_PWD/../Tools/Timer/ -lTimer

INCLUDEPATH += $$PWD/../Tools/Timer
DEPENDPATH += $$PWD/../Tools/Timer

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/Timer/release/libTimer.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/Timer/debug/libTimer.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/Timer/release/Timer.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/Timer/debug/Timer.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../Tools/Timer/libTimer.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Grammar/release/ -lGrammar
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Grammar/debug/ -lGrammar
else:unix: LIBS += -L$$OUT_PWD/../Grammar/ -lGrammar

INCLUDEPATH += $$PWD/../Grammar
DEPENDPATH += $$PWD/../Grammar

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Grammar/release/libGrammar.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Grammar/debug/libGrammar.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Grammar/release/Grammar.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Grammar/debug/Grammar.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../Grammar/libGrammar.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../SuffixArrayLib/release/ -lSuffixArrayLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../SuffixArrayLib/debug/ -lSuffixArrayLib
else:unix: LIBS += -L$$OUT_PWD/../SuffixArrayLib/ -lSuffixArrayLib

INCLUDEPATH += $$PWD/../SuffixArrayLib
DEPENDPATH += $$PWD/../SuffixArrayLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../SuffixArrayLib/release/libSuffixArrayLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../SuffixArrayLib/debug/libSuffixArrayLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../SuffixArrayLib/release/SuffixArrayLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../SuffixArrayLib/debug/SuffixArrayLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../SuffixArrayLib/libSuffixArrayLib.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Tools/Types/release/ -lTypes
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Tools/Types/debug/ -lTypes
else:unix: LIBS += -L$$OUT_PWD/../Tools/Types/ -lTypes

INCLUDEPATH += $$PWD/../Tools/Types
DEPENDPATH += $$PWD/../Tools/Types

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/Types/release/libTypes.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/Types/debug/libTypes.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/Types/release/Types.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../Tools/Types/debug/Types.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../Tools/Types/libTypes.a
