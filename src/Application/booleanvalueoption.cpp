#include "booleanvalueoption.h"

BooleanValueOption::BooleanValueOption(QString name, QString defaultParameter)
    : Option(name, defaultParameter)
{

}

BooleanValueOption::~BooleanValueOption()
{

}

QString BooleanValueOption::getAvailableParameterInfo()
{
    return Option::getAvailableParameterInfo() + "T (turns on) | F (turns off). ";
}

QString BooleanValueOption::getInvalidParameterInfo()
{
    return QString("'%1' is an invalid value. Try T or F.").arg(parameter);
}

bool BooleanValueOption::isParameterValid()
{
    return (parameter == "T") || (parameter == "F");
}

bool BooleanValueOption::isParameterTrue()
{
    return parameter == "T";
}
