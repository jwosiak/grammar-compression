#include "compressoroption.h"

CompressorOption::CompressorOption()
    : Option("--compr", "AVL")
{

}

QString CompressorOption::getUsageInfo()
{
    return Option::getUsageInfo() + "Compression algorithm.";
}

QString CompressorOption::getAvailableParameterInfo()
{
    return Option::getAvailableParameterInfo()
            + "AVL | Alpha | Recompress "
            + "| Simple | LZ1 | LZ77. Default: AVL.";
}

bool CompressorOption::isParameterValid()
{
    static const QString availableValues[] = { "AVL", "Alpha", "Recompress",
                                               "Simple", "LZ1", "LZ77" };
    for (const QString &availableValue : availableValues)
        if (parameter == availableValue)
            return true;
    return false;
}

QString CompressorOption::getInvalidParameterInfo()
{
    return QString("'%1' is not an available name of compression algorithm.")
            .arg(parameter);
}
