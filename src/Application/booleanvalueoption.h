#ifndef BOOLEANVALUEOPTION_H
#define BOOLEANVALUEOPTION_H

#include "option.h"

class BooleanValueOption : public Option
{
protected:
    BooleanValueOption(QString name, QString defaultParameter);

public:
    virtual ~BooleanValueOption();
    virtual QString getAvailableParameterInfo();
    virtual QString getInvalidParameterInfo();
    virtual bool isParameterValid();
    bool isParameterTrue();
};

#endif // BOOLEANVALUEOPTION_H
