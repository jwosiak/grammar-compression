#include "helpoption.h"

HelpOption::HelpOption(std::vector<Option *> &optionsToHelp)
    : Option("--help", "")
    , optionsToHelp(optionsToHelp)
{

}

QString HelpOption::getUsageInfo()
{
    QString content;
    content += "Usage:\n   GrammarCompression [ option ]* argument\n";
    content += "Options (each takes a parameter):\n";
    for (Option *opt : optionsToHelp) {
        content += "   " + opt->getUsageInfo() + "\n";
        content += "   " + opt->getAvailableParameterInfo() + "\n";
    }
    content += "Argument:\n   A path to input file.\n";
    return content;
}

QString HelpOption::getInvalidParameterInfo()
{
    return "";
}

bool HelpOption::isParameterValid()
{
    return true;
}
