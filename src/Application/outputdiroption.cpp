#include "outputdiroption.h"
#include <QDir>

OutputDirOption::OutputDirOption()
    : Option("--out", "./")
{

}

QString OutputDirOption::getUsageInfo()
{
    return Option::getUsageInfo() + "  Output directory of generated files.";
}

QString OutputDirOption::getAvailableParameterInfo()
{
    return Option::getAvailableParameterInfo() + "a valid path to directory. "
            + "Default: the current directory.";
}

bool OutputDirOption::isParameterValid()
{
    QDir pathDir(parameter);
    return pathDir.exists();
}

QString OutputDirOption::getInvalidParameterInfo()
{
    return QString("'%1' is not a valid directory path.").arg(parameter);
}
