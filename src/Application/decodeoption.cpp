#include "decodeoption.h"

DecodeOption::DecodeOption()
    : BooleanValueOption ("--dec", "F")
{

}

QString DecodeOption::getUsageInfo()
{
    return Option::getUsageInfo()
            + "  If set, then the program decompresses input "
            + "(argument must be a path to grammar file) "
            + "instead of compressing it.";
}

QString DecodeOption::getAvailableParameterInfo()
{
    return BooleanValueOption::getAvailableParameterInfo()
            + "Default: F.";
}
