#include "statsgenerator.h"
#include "grammar.h"
#include "factor.h"
#include "grammarstatsevaluator.h"
#include "iotools.h"

StatsGenerator::StatsGenerator(QString outputPath)
    : outputPath(outputPath)
{

}

StatsGenerator::~StatsGenerator()
{
    for (auto item : nonterminalsDetailedStats)
        delete item.second;
}

void StatsGenerator::computeStatsForGrammar(const Grammar &grammar)
{
    GrammarStatsEvaluator statsEvaluator(grammar);
    nonterminalsDetailedStats = statsEvaluator.produceStats();
}

void StatsGenerator::writeGrammarDetailedStats()
{
    Tools::writeStatsToCSV(outputPath + "/symbols_stats.csv",
                           nonterminalsDetailedStats);
}

void StatsGenerator::writeGrammarGeneralStats()
{
    auto anyNonterminalsStats = nonterminalsDetailedStats["Height"];
    index_t maxHeight = anyNonterminalsStats->back();
    Tools::writeGeneralStatsToJSON(
            outputPath + "/general_stats.json", compressionTime,
                inputWordSize, maxHeight, anyNonterminalsStats->size());
}

void StatsGenerator::writeFactorizationGeneralStats(
        const std::vector<Factor> &factorization)
{
    Tools::writeGeneralStatsToJSON(
            outputPath + "/general_stats.json", compressionTime,
                inputWordSize, 0, factorization.size());
}

void StatsGenerator::setCompressionTime(unsigned int value)
{
    compressionTime = value;
}

void StatsGenerator::setInputWordSize(const index_t &value)
{
    inputWordSize = value;
}
