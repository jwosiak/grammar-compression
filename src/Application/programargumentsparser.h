#ifndef PROGRAMARGUMENTSPARSER_H
#define PROGRAMARGUMENTSPARSER_H

#include "option.h"
#include "booleanvalueoption.h"
#include "helpoption.h"
#include "parseerrorexception.h"
#include <map>
#include <QString>

class ProgramArgumentsParser
{
    std::map<QString, Option*> nameToOption;
    HelpOption *helpOption;
    bool writeHelpFlag;
public:
    ProgramArgumentsParser();
    ~ProgramArgumentsParser();

    void parseOptions(int argc, char *argv[]) noexcept(false);
    QString parseAndGetArgument(int argc, char *argv[]) noexcept(false);
    QString getOptionParam(QString optionName);

    bool isOptionSetTrue(QString optionName);
    bool isHelpToBeWritten() const;
    QString getHelpContent();

    float getAlphaParameter();
private:
    bool isOptionNotRegistered(QString optionName);
    bool hasFormatOfOptionName(QString name);
    Option *getOptionByName(QString optionName) noexcept(false);
    void raiseMissingParam(Option *optionWithMissingParam) noexcept(false);
    void raiseInvalidParam(Option *optionWithInvalidParam) noexcept(false);
};

#endif // PROGRAMARGUMENTSPARSER_H
