#include "parseerrorexception.h"

ParseErrorException::ParseErrorException(QString message)
    : message(message.toStdString())
{

}

const char *ParseErrorException::what() const noexcept
{
    return message.c_str();
}
