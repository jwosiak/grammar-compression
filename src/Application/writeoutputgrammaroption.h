#ifndef WRITEOUTPUTGRAMMAROPTION_H
#define WRITEOUTPUTGRAMMAROPTION_H

#include "booleanvalueoption.h"

class WriteOutputGrammarOption : public BooleanValueOption
{
public:
    WriteOutputGrammarOption();

    QString getUsageInfo();
    QString getAvailableParameterInfo();
};

#endif // WRITEOUTPUTGRAMMAROPTION_H
