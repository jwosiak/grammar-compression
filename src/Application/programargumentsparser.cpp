#include "programargumentsparser.h"

#include "compressoroption.h"
#include "outputdiroption.h"
#include "writegeneralstatsoption.h"
#include "writedetailedstatsoption.h"
#include "writeoutputgrammaroption.h"
#include "decodeoption.h"
#include "alphaparameteroption.h"
#include "helpoption.h"

#include <QFileInfo>
#include <string>

ProgramArgumentsParser::ProgramArgumentsParser()
    : writeHelpFlag(false)
{
    std::vector<Option*> handledOptions = { new CompressorOption(),
                                            new OutputDirOption(),
                                            new WriteGeneralStatsOption(),
                                            new WriteDetailedStatsOption(),
                                            new WriteOutputGrammarOption(),
                                            new DecodeOption(),
                                            new AlphaParameterOption() };

    for (Option *opt : handledOptions)
        nameToOption[opt->getName()] = opt;

    helpOption = new HelpOption(handledOptions);
}

ProgramArgumentsParser::~ProgramArgumentsParser()
{
    for (auto item : nameToOption)
        delete item.second;
    delete helpOption;
}

void ProgramArgumentsParser::parseOptions(int argc, char *argv[])
{
    writeHelpFlag = (argc >= 2) && (QString(argv[1]) == "--help");

    for (int i = 1; i < argc - 1; i += 2) {
        QString currentArgument(argv[i]);

        if (currentArgument == "--help")
            writeHelpFlag = true;
        if (writeHelpFlag)
            break;

        if (hasFormatOfOptionName(currentArgument)) {
            Option *optionToSet = getOptionByName(currentArgument);

            // the last item of argv is the argument
            if ((i+1 == argc-1))
                raiseMissingParam(optionToSet);

            QString optionsParameter = QString(argv[i+1]);
            if (hasFormatOfOptionName(optionsParameter))
                raiseMissingParam(optionToSet);

            optionToSet->setParameter(optionsParameter);
            if (!optionToSet->isParameterValid())
                raiseInvalidParam(optionToSet);
        }
    }
}

QString ProgramArgumentsParser::parseAndGetArgument(int argc, char *argv[])
{
    if (argc <= 1)
        throw ParseErrorException(
                    QString("A path to input text file must be given. ")
                            + "Use option --help to get more information"
                            + " about usage.\n");

    QString inputPath = argv[argc-1];
    QFileInfo inputPathInfo(inputPath);
    if (!inputPathInfo.exists() || !inputPathInfo.isFile())
        throw ParseErrorException(
                    QString("Argument error: '%1' is not a valid path")
                    .arg(inputPath) + " to text file.\n");

    return inputPath;
}

QString ProgramArgumentsParser::getOptionParam(QString optionName)
{
    return nameToOption[optionName]->getParameter();
}

bool ProgramArgumentsParser::isOptionSetTrue(QString optionName)
{
    return static_cast<BooleanValueOption*>(nameToOption[optionName])
            ->isParameterTrue();
}


bool ProgramArgumentsParser::isHelpToBeWritten() const
{
    return writeHelpFlag;
}

QString ProgramArgumentsParser::getHelpContent()
{
    return helpOption->getUsageInfo();
}

float ProgramArgumentsParser::getAlphaParameter()
{
    return std::stof(getOptionParam("--alpha").toStdString());
}

bool ProgramArgumentsParser::isOptionNotRegistered(QString optionName)
{
    if (nameToOption.find(optionName) == nameToOption.end())
        return true;
    return false;
}

bool ProgramArgumentsParser::hasFormatOfOptionName(QString name)
{
    return (name.size() > 2) && (name[0] == '-') && (name[1] == '-');
}

Option *ProgramArgumentsParser::getOptionByName(QString optionName)
{
    if (isOptionNotRegistered(optionName))
        throw ParseErrorException(
                QString("'%1' is not a valid option.\n").arg(optionName));

    return nameToOption[optionName];

}

void ProgramArgumentsParser::raiseMissingParam(Option *optionWithMissingParam)
{
    throw ParseErrorException(
            QString("The parameter of '%1' option is missing.\n")
                .arg(optionWithMissingParam->getName()));
}

void ProgramArgumentsParser::raiseInvalidParam(Option *optionWithInvalidParam)
{
    throw ParseErrorException(
                QString("%1 error: %2\n")
                .arg(optionWithInvalidParam->getName())
                .arg(optionWithInvalidParam->getInvalidParameterInfo()));
}
