#include "writedetailedstatsoption.h"

WriteDetailedStatsOption::WriteDetailedStatsOption()
    : BooleanValueOption("--dstat", "F")

{

}

QString WriteDetailedStatsOption::getUsageInfo()
{
    return Option::getUsageInfo()
            + "If set, then the program outputs detailed stats of each nonterminal.";
}

QString WriteDetailedStatsOption::getAvailableParameterInfo()
{
    return BooleanValueOption::getAvailableParameterInfo()
            + "Default: F.";
}
