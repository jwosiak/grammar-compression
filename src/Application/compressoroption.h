#ifndef COMPRESSOROPTION_H
#define COMPRESSOROPTION_H

#include "option.h"

class CompressorOption : public Option
{
public:
    CompressorOption();

    QString getUsageInfo();
    QString getAvailableParameterInfo();
    bool isParameterValid();
    QString getInvalidParameterInfo();
};

#endif // COMPRESSOROPTION_H
