#ifndef WRITEGENERALSTATSOPTION_H
#define WRITEGENERALSTATSOPTION_H

#include "booleanvalueoption.h"

class WriteGeneralStatsOption : public BooleanValueOption
{
public:
    WriteGeneralStatsOption();

    QString getUsageInfo();
    QString getAvailableParameterInfo();
};

#endif // WRITEGENERALSTATSOPTION_H
