#include "writeoutputgrammaroption.h"

WriteOutputGrammarOption::WriteOutputGrammarOption()
    : BooleanValueOption ("--grdef", "T")
{

}

QString WriteOutputGrammarOption::getUsageInfo()
{
    return Option::getUsageInfo()
            + "If set, then the program outputs the definition of generated grammar.";
}

QString WriteOutputGrammarOption::getAvailableParameterInfo()
{
    return BooleanValueOption::getAvailableParameterInfo()
            + "Default: T.";
}
