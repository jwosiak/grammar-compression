#ifndef WRITEDETAILEDSTATSOPTION_H
#define WRITEDETAILEDSTATSOPTION_H

#include "booleanvalueoption.h"

class WriteDetailedStatsOption : public BooleanValueOption
{
public:
    WriteDetailedStatsOption();

    QString getUsageInfo();
    QString getAvailableParameterInfo();
};

#endif // WRITEDETAILEDSTATSOPTION_H
