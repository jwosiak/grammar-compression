#ifndef ALPHAPARAMETEROPTION_H
#define ALPHAPARAMETEROPTION_H

#include "option.h"

class AlphaParameterOption : public Option
{
public:
    AlphaParameterOption();

    QString getUsageInfo();
    QString getAvailableParameterInfo();
    QString getInvalidParameterInfo();
    bool isParameterValid();
};

#endif // ALPHAPARAMETEROPTION_H
