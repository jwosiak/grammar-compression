#include "option.h"

Option::Option(QString name, QString defaultParameter)
    : name(name)
    , parameter(defaultParameter)
{

}

Option::~Option()
{

}

bool Option::hasName(QString name)
{
    return this->name == name;
}

QString Option::getUsageInfo()
{
    return name + "     ";
}

QString Option::getAvailableParameterInfo()
{
    return "            Parameter: ";
}

void Option::setParameter(QString parameter)
{
    this->parameter = parameter;
}

QString Option::getParameter() const
{
    return parameter;
}

QString Option::getName() const
{
    return name;
}
