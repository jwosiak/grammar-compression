TEMPLATE = subdirs

SUBDIRS += \
    Types \
    Grammar \
    SuffixArrayLib \
    Timer \
    GenTools \
    GrammarCompressor \
    IOTools \
    Application \
    Tests \

CONFIG += ordered

GenTools.subdir = Tools/GenTools
IOTools.subdir = Tools/IOTools
Types.subdir = Tools/Types
Timer.subdir = Tools/Timer
